<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="theme-color" content="#02AEF0">
    <title><?php echo !empty($html['title'])?$html['title']:''; ?></title>
    <link rel="icon" href="<?php echo $this->config->item('template_uri'); ?>favicon.ico" type="image/x-icon">
    <?php if(isset($html['css'])): echo $html['css']; endif;?>
    <?php if(isset($template['newcss'])): echo $template['newcss']; endif;?>
    <?php if(isset($html['js_head'])): echo $html['js_head']; endif;?>
</head>
<body class="theme-pgn">
<?php if(isset($template['header'])): echo $template['header']; endif;?>
<?php if(isset($content['content'])): echo $content['content']; endif;?>
<?php if(isset($html['js_content'])): echo $html['js_content']; endif;?>
</body>
</html>