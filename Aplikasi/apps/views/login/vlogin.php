<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo !empty($html['title'])?$html['title']:''; ?> | Administrator</title>
    <link rel="icon" href="<?php echo $this->config->item('template_uri'); ?>favicon.ico" type="image/x-icon">
    <?php echo $html['css'];?>
    <style>
        .login-page .login-box .logo a{font-size:25px !important;;}
        .login-page {background-color: #02AEF0 !important;}
        .bg-pgn{background-color:#02AEF0; color:#FFF;}
        .btn:hover, .btn:focus, .btn.focus {color: #fff !important;text-decoration: none;}
    </style>
    <?php echo $html['js_head'];?>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body class="login-page">
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);" style="cursor: default;">
            <b>PGN Perkasa</b>
        </a>
    </div>
    <div class="card">
        <div class="body">
            <form id="sign_in" method="POST" action="<?php echo base_url().$this->router->fetch_class().'/process'?>">
                <div class="msg"><?php echo $this->session->flashdata("msg"); ?></div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="idc" placeholder="Masukan ID Karyawan" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
						<div name="captcha" class="g-recaptcha" data-sitekey="6LdYY7IZAAAAAFGqT6xNoZxUhu77aKpYCVwoyhcL"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-block bg-pgn waves-effect" type="submit">SIGN IN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $html['js_content'];?>
</body>
</html>