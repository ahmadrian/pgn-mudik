<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <span class="text-left"><?php $sess = $this->session->userdata('user_data'); echo ucwords($sess['karyawan']);?></span>
                        </h2>
                    </div>
                    <div clas="body">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Info Transportasi</th>
                                <th width="15%" class="text-center">Status</th>
                                <th width="15%" class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($list)): ?>
                                <?php for($i=0; $i<count($list); $i++):
                                    $status[$i] = $list[$i]['status'];
                                    if($status[$i] == 0){
                                        $show_status[$i] = "<h5><span class='label bg-green'>Terverifikasi</span></h5>";
                                    }elseif ($status[$i] == 1){
                                        $show_status[$i] = "<h5><span class='label bg-red'>Rejected</span></h5>";
                                    }elseif ($status[$i] == 2){
                                        $show_status[$i] = "<h5><span class='label bg-purple'>Waiting</span></h5>";
                                    }else{
                                        $show_status[$i] = "<h5><span class='label bg-red'>Tidak Diketahui</span></h5>";
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            Moda : <b><?php echo $list[$i]['jenis']?>/<?php echo $list[$i]['moda']?></b>
                                            <br>
                                            <br>
                                            Rute : <?php echo $list[$i]['rute_awal']. '-' .$list[$i]['rute_tujuan'];?>
                                            <br>
                                            Jadwal Keberangkatan : <?php echo $list[$i]['jadwal'];?>
                                        </td>
                                        <td class="text-center"><?php echo $show_status[$i];?></td>
                                        <td class="text-center">
                                            <?php if($list[$i]['status'] == 0):?>
                                                <a target="_blank" href="<?php echo base_url().'tiket/cetaktiket/'.$list[$i]['id_booking']?>" class="btn bg-pgn btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak Tiket <?php echo $list[$i]['karyawan']; ?>">
                                                    <i class="material-icons">print</i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr><td colspan="5" class="text-center">Data Kosong</td></tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>