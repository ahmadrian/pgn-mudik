<style>
    .theme-pgn .navbar {
        background-color: #02AEF0;
    }

    .theme-pgn .navbar-brand {
        color: #fff;
    }

    .theme-pgn .navbar-brand:hover {
        color: #fff;
    }

    .theme-pgn .navbar-brand:active {
        color: #fff;
    }

    .theme-pgn .navbar-brand:focus {
        color: #fff;
    }

    .theme-pgn .nav > li > a {
        color: #fff;
    }

    .theme-pgn .nav > li > a:hover {
        background-color: transparent;
    }

    .theme-pgn .nav > li > a:focus {
        background-color: transparent;
    }

    .theme-pgn .nav .open > a {
        background-color: transparent;
    }

    .theme-pgn .nav .open > a:hover {
        background-color: transparent;
    }

    .theme-pgn .nav .open > a:focus {
        background-color: transparent;
    }

    .theme-pgn .bars {
        color: #fff;
    }

    .theme-pgn .sidebar .menu .list li.active {
        background-color: transparent;
    }

    .theme-pgn .sidebar .menu .list li.active > :first-child i, .theme-pgn .sidebar .menu .list li.active > :first-child span {
        color: #02AEF0;
    }

    .theme-pgn .sidebar .menu .list .toggled {
        background-color: transparent;
    }

    .theme-pgn .sidebar .menu .list .ml-menu {
        background-color: transparent;
    }

    .theme-pgn .sidebar .legal {
        background-color: #fff;
    }

    .theme-pgn .sidebar .legal .copyright a {
        color: #02AEF0 !important;
    }

    .col-pgn{color:#02AEF0;}
    .bg-pgn{background-color:#02AEF0; color:#FFF;}
    .btn:hover, .btn:focus, .btn.focus {
        color: #fff !important;
        text-decoration: none;
    }
    section.content {margin: 100px 15px 0 25px !important;}
</style>