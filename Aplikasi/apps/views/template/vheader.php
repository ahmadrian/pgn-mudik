<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<div class="overlay"></div>
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a class="navbar-brand" href="javascript:">Aplikasi Mudik PGN</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class=""><a href="<?php echo site_url('dashboard');?>" title="Logout"><i class="material-icons"></i>Home</a></li>
                <li class=""><a href="<?php echo site_url('tiket');?>" title="Logout"><i class="material-icons"></i>Tiket</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="pull-right"><a href="<?php echo site_url('logout');?>" title="Logout"><i class="material-icons">power_settings_new</i></a></li>
            </ul>
        </div>
    </div>
</nav>