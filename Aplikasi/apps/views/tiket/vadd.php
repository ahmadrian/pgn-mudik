<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo $list[0]['rute_awal'].' - '.$list[0]['rute_tujuan'];?>
                            <br>
                            <?php echo $list[0]['moda'];?>
                        </h2>
                    </div>
                    <div class="body table-responsive">
                        <form id="form_validation" method="POST" action="<?php echo base_url().$this->router->fetch_class().'/order/'.$this->uri->segment(3).'/'.$this->uri->segment(4);?>">
                            <input type="hidden" name="id_transportasi" value="<?php echo $this->uri->segment(3);?>"/>
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" name="jumlah_penumpang" class="form-control" placeholder="Jumlah Penumpang" max="<?php echo $max_slot;?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">Entry data penumpang</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>