<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>List Transportasi Mudik PGN</h2>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>Info Transportasi</th>
                                <th width="15%" class="text-center">Jumlah Kapasitas</th>
                                <th width="15%" class="text-center">Slot Tersisa</th>
                                <th width="15%" class="text-center">Status</th>
                                <th width="15%" class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($list)): ?>
                                <?php for($i=0; $i<count($list); $i++):
                                    $status[$i] = $list[$i]['status'];
                                    if($status[$i] == 0){
                                        $show_status[$i] = "<h5><span class='label bg-green'>Kapasitas Tersedia</span></h5>";
                                    }elseif ($status[$i] == 1){
                                        $show_status[$i] = "<h5><span class='label bg-red'>Kapasitas Penuh</span></h5>";
                                    }else{
                                        $show_status[$i] = "<h5><span class='label bg-red'>Tidak Diketahui</span></h5>";
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            Moda : <b><?php echo $list[$i]['jenis']?>/<?php echo $list[$i]['moda']?></b>
                                            <br>
                                            <br>
                                            Rute : <?php echo $list[$i]['rute_awal']. '-' .$list[$i]['rute_tujuan'];?>
                                            <br>
                                            Jadwal Keberangkatan : <?php echo $list[$i]['jadwal'];?>
                                        </td>
                                        <td class="text-center"><?php echo $list[$i]['slot'];?></td>
                                        <td class="text-center">
                                            <?php
                                            $get_booking[$i] = $this->mtiket->get_booking_by_transportasi($list[$i]['id_transportasi']);
                                            if(count($get_booking[$i]) > 0){
                                                $jp = 0;
                                                for($j=0; $j<count($get_booking[$i]); $j++){
                                                    $jp += $get_booking[$i][$j]['jumlah_penumpang'];
                                                }
                                                echo $slot_ready = $list[$i]['slot']-$jp;
                                            }else{
                                                echo $slot_ready = $list[$i]['slot'];
                                            }
                                            ?>
                                        </td>
                                        <td class="text-center"><?php echo $show_status[$i];?></td>
                                        <td class="text-center">
                                            <a href="<?php echo base_url().$this->router->fetch_class().'/order/'.$list[$i]['id_transportasi'].'/'.$slot_ready?>" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="" data-original-title="Order <?php echo $list[$i]['rute_awal'] .'-'.$list[$i]['rute_tujuan']; ?>">
                                                <i class="material-icons">border_color</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr><td colspan="5" class="text-center">Data Kosong</td></tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>