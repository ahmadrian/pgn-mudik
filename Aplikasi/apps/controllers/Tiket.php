<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tiket extends MY_Controller
{
    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->load->library("libpgn");
        $this->load->library("alert");
        $this->load->library('ciqrcode');
        $this->load->model("tiket/mtiket");
    }

    function index()
    {
        $a['html']['title'] = 'List Transportasi '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $start = 0;
        if($this->uri->segment(2)){
            $start = $this->uri->segment(2);
        }

        $t['list'] = $this->mtiket->get_data($start, $this->limit);
        $t['totalData'] = $this->mtiket->get_num_data();

        $create_paging = '';
        if($t['totalData'] > 0){
            $create_paging = $this->libpgn->pagination($t['totalData'],$this->limit);
        }
        $t['create_paging'] = $create_paging;
        
        $a['content']['content'] = $this->load->view('tiket/vtiket', $t, TRUE);

        $this->load->view('pages/vtiket_pages', $a, FALSE);
    }

    function order()
    {
        $id = $this->uri->segment(3);
        if(empty($id)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Uri tidak diketahui"));
            redirect(base_url().$this->router->fetch_class());
        }

        if($this->input->post()){
            $msg = '';
            if($this->input->post('jumlah_penumpang') == NULL){ $msg .= "<p>Jumlah Penumpang kosong</p>"; }
            if($this->input->post('id_transportasi') == NULL){ $msg .= "<p>Maaf permintaan tidak dapat diproses</p>"; }
            if($this->input->post('jumlah_penumpang') == NULL || $this->input->post('id_transportasi') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class().'/order/'.$id.'/'.$this->uri->segment(4));
            }

            /*check apakah sudah order moda yang sama*/
            $sess = $this->session->userdata('user_data');
            $check = $this->mtiket->check_exist($sess['id_karyawan'], $id);
            if($check > 0){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Maaf,  <strong>".ucfirst($sess['karyawan'])."</strong> sudah memesan tiket yang sama"));
                redirect(base_url().$this->router->fetch_class());
            }

            $id_booking = $this->mtiket->get_new_id('t_booking','id_booking');
            $idk = $sess['id_karyawan'];
            $idt = $this->input->post('id_transportasi');
            $jp = $this->input->post('jumlah_penumpang');
            $this->mtiket->order($id_booking,$idk,$idt,$jp,2);
            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Pesanan Tiket Anda dengan Nomor #". $id_booking ." Berhasil dibuat, silahkan cek email atau melihat status tiket pesanan Anda pada menu (tiket saya)"));
            redirect(base_url().$this->router->fetch_class().'/tiketsaya/'.$id_booking.'/'.$this->uri->segment(4));
        }

        $a['html']['title'] = 'Order Tiket '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $id = $this->uri->segment(3);
        $t['list'] = $this->mtiket->get_data_by_id($id);
        $t['max_slot'] = $this->uri->segment(4);

        $a['content']['content'] = $this->load->view('tiket/vadd', $t, TRUE);

        $this->load->view('pages/vtiket_pages', $a, FALSE);
    }

    function tiketsaya()
    {
        $id = $this->uri->segment(3);
        if(empty($id)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Uri tidak diketahui"));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Order Tiket '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $id = $this->uri->segment(3);
        $t['list'] = $this->mtiket->get_booking_by_id($id);

        $a['content']['content'] = $this->load->view('tiket/vstatus', $t, TRUE);

        $this->load->view('pages/vtiket_pages', $a, FALSE);
    }

    function cetaktiket(){
        $id = $this->uri->segment(3);
        if (empty($id)) {
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Uri tidak diketahui"));
            redirect(base_url() . $this->router->fetch_class());
        }

        $a['html']['title'] = 'Detail Booking ' . $this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $get_detail = $this->mtiket->get_booking_by_id($id);
        $t['list'] = $get_detail;

        $qrcodeUrl = 'http://mudik.ahmadrian.com/absen/'.$get_detail[0]['id_booking'];
        $image_name = $get_detail[0]['id_booking'].'.jpg';

        $params['data'] = $qrcodeUrl;
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = $this->config->item('upload_foto_qrcode').$image_name;
        $this->ciqrcode->generate($params);
        $t['qrcode'] = $this->config->item('images_uri_qrcode').$image_name;

        $a['content']['content'] = $this->load->view('tiket/vcetak', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vtiket_pages', $a, FALSE);
    }

}