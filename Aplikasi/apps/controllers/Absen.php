<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library("libpgn");
        $this->load->library("alert");
        $this->load->model("api/mapi");
    }

    function index()
    {
        $id = $this->uri->segment(2);
        if(empty($id)){
            redirect(base_url());
        }

        $a['html']['title'] = 'Absen '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $t['get_api'] = $this->mapi->checkin($id);

        $a['content']['content'] = $this->load->view('absen/vabsen', $t, TRUE);

        $this->load->view('pages/vdashboard_pages', $a, FALSE);
    }

}