<?php

class Login extends CI_Controller
{

    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->library("alert");
        $this->load->model("mlogin");
    }

    function index()
    {
        $a['html']['title'] = $this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] = add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');

        $this->load->view('login/vlogin', $a, FALSE);
    }

    function process()
    {
        if (isset($_POST['g-recaptcha-response'])) {
            $secret = '6LdYY7IZAAAAALina_38WpcyKWVqoGAyWCqVL99K';
            $response = $_POST["g-recaptcha-response"];
            $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            $captcha_success = json_decode($verify);

            if ($captcha_success->success == true) {
                $idc = addslashes(trim($this->input->post('idc')));
                $check_login = $this->mlogin->get_data($idc);

                if (!empty($check_login)) {
                    $user_session = array(
                        'id_karyawan' => $check_login[0]['id_karyawan'],
                        'id_card' => $check_login[0]['id_card'],
                        'karyawan' => $check_login[0]['karyawan'],
                        'email' => $check_login[0]['email'],
                    );
                    $this->session->set_userdata("user_data", $user_session);

                    $this->session->set_flashdata("msg", $this->alert->alertMsg("success", "Selamat Datang " . ucwords($check_login[0]['karyawan'])));
                    redirect(base_url("dashboard"));
                } else {
                    $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "ID Karyawan tidak diketahui"));
                    redirect(base_url());
                }

            } else {
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Captcha tidak sesuai"));
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Captcha tidak ditemukan"));
            redirect(base_url());
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect();
    }
}