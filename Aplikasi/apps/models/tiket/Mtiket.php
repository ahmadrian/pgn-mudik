<?php

class Mtiket extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get_booking_by_transportasi($id)
    {
        $sql = "SELECT 
                id_booking, id_transportasi, jumlah_penumpang 
                FROM t_booking 
                WHERE
                (status = 0 OR status = 2 ) AND 
                id_transportasi = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT 
                a.id_transportasi, a.id_jenis_moda, a.id_moda, a.rute_awal, a.rute_tujuan, a.id_jadwal, a.content, a.status,
                b.jenis,
                c.moda, c.slot,
                d.rute AS rute_awal,
                e.rute AS rute_tujuan,
                f.jadwal
                FROM t_transportasi AS a 
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis_moda
                LEFT JOIN t_moda AS c ON c.id_moda = a.id_moda
                LEFT JOIN t_rute AS d ON d.id_rute = a.rute_awal
                LEFT JOIN t_rute AS e ON e.id_rute = a.rute_tujuan
                LEFT JOIN t_jadwal AS f ON f.id_jadwal = a.id_jadwal
                ORDER BY id_transportasi DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_transportasi FROM t_transportasi ORDER BY id_transportasi DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT 
                a.id_transportasi, a.id_jenis_moda, a.id_moda, a.rute_awal, a.rute_tujuan, a.id_jadwal, a.content, a.status,
                b.jenis,
                c.moda, c.slot,
                d.rute AS rute_awal,
                e.rute AS rute_tujuan,
                f.jadwal
                FROM t_transportasi AS a 
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis_moda
                LEFT JOIN t_moda AS c ON c.id_moda = a.id_moda
                LEFT JOIN t_rute AS d ON d.id_rute = a.rute_awal
                LEFT JOIN t_rute AS e ON e.id_rute = a.rute_tujuan
                LEFT JOIN t_jadwal AS f ON f.id_jadwal = a.id_jadwal 
                WHERE a.id_transportasi = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function check_exist($idk, $idt){
        $sql = "SELECT id_booking FROM t_booking WHERE id_karyawan = '". $idk ."' AND id_transportasi = ".$idt;

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function order($id_booking,$idk,$idt,$jp,$status)
    {
        $sql = "INSERT INTO t_booking (id_booking,id_karyawan, id_transportasi, jumlah_penumpang, status) VALUES('". $id_booking ."','". $idk ."', '". $idt ."', '". $jp ."', '". $status ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function get_booking_by_id($id)
    {
        $sql = "SELECT 
                a.id_booking, a.id_transportasi, a.jumlah_penumpang, a.status, a.keterangan,
                b.karyawan, b.id_card,
                d.jenis,
                e.moda, e.slot,
                f.rute AS rute_awal,
                g.rute AS rute_tujuan,
                h.jadwal
                FROM t_booking AS a 
                LEFT JOIN t_karyawan AS b ON b.id_karyawan = a.id_karyawan
                LEFT JOIN t_transportasi AS c ON c.id_transportasi = a.id_transportasi
                LEFT JOIN t_jenis_moda AS d ON d.id_jenis_moda = c.id_jenis_moda
                LEFT JOIN t_moda AS e ON e.id_moda = c.id_moda
                LEFT JOIN t_rute AS f ON f.id_rute = c.rute_awal
                LEFT JOIN t_rute AS g ON g.id_rute = c.rute_tujuan
                LEFT JOIN t_jadwal AS h ON h.id_jadwal = c.id_jadwal
                WHERE a.id_booking = ".$id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_booking_by_karyawan($id)
    {
        $sql = "SELECT 
                a.id_booking, a.id_transportasi, a.jumlah_penumpang, a.status, a.keterangan,
                b.karyawan, b.id_card,
                d.jenis,
                e.moda, e.slot,
                f.rute AS rute_awal,
                g.rute AS rute_tujuan,
                h.jadwal
                FROM t_booking AS a 
                LEFT JOIN t_karyawan AS b ON b.id_karyawan = a.id_karyawan
                LEFT JOIN t_transportasi AS c ON c.id_transportasi = a.id_transportasi
                LEFT JOIN t_jenis_moda AS d ON d.id_jenis_moda = c.id_jenis_moda
                LEFT JOIN t_moda AS e ON e.id_moda = c.id_moda
                LEFT JOIN t_rute AS f ON f.id_rute = c.rute_awal
                LEFT JOIN t_rute AS g ON g.id_rute = c.rute_tujuan
                LEFT JOIN t_jadwal AS h ON h.id_jadwal = c.id_jadwal
                WHERE a.id_karyawan = ".$id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}