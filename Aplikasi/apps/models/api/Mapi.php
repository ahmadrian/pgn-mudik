<?php

class Mapi extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function checkin($id)
    {
        $url = 'http://apimudik.ahmadrian.com/absen/'.$id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);
        curl_close($ch);

        $decode = json_decode($data, TRUE);
        $result = $decode['data'];
        return $result;
    }
}