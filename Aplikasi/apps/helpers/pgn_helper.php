<?php
function add_js($js = '')
{
    $CI = &get_instance();
    $url = $CI->config->item('template_uri');
    $string = '<script src="' . $url . $js . '"></script>';
    return $string;
}

function m_add_js($js)
{
    $CI = &get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<script src="' . $url . $js . '"></script>';
    return $string;
}

function add_external_js($js = '')
{
    $string = '<script src="' . $js . '"></script>';
    return $string;
}

function add_css($css = '')
{
    $CI = &get_instance();
    $url = $CI->config->item('template_uri');
    $string = '<link rel="stylesheet" type="text/css" href="' . $url . $css . '" media="all">';
    return $string;
}

function m_add_css($css = '')
{
    $CI = &get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<link rel="stylesheet" type="text/css" href="' . $url . $css . '" media="all">';
    return $string;
}

function add_external_css($css = '')
{
    $string = '<link rel="stylesheet" type="text/css" href="' . $css . '">';
    return $string;
}

function template_uri()
{
    $CI = &get_instance();
    return $CI->config->item('template_uri');
}

function m_template_uri()
{
    $CI = &get_instance();
    return $CI->config->item('m_template_uri');
}

function images_uri()
{
    $CI = &get_instance();
    return $CI->config->item('images_uri');
}

function xDebug($str)
{
    echo '<pre>';
    print_r($str);
    echo '</pre>';
}