<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'absen/missing';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['absen/(:num)'] = 'absen';
