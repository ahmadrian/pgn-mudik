<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("mabsen");
    }

    function index(){
        $id = $this->uri->segment(2);
        if(empty($id)){
            $result = array('success' => 0,'error' => 1,'data' => array('result' => 'are u missing something?'));
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }else{
            $get_data = $this->mabsen->get_absen($id);
            $data = $get_data;
            if(count($data) > 0){
                $cekabsenmasuk = $this->mabsen->cekabsenmasuk($data[0]['id_booking']);
                if(empty($cekabsenmasuk[0])){
                    $this->mabsen->check_in($data[0]['id_booking']);
                }

                $info = $this->getInfo();
                $result = array(
                    'server' => $info['ip'],
                    'data' => $data
                );
                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            }else{
                $result = array('success' => 0,'error' => 1,'data' => array('result' => 'are u missing something?'));
                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            }
        }
    }

    function getInfo(){
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $dataurl = parse_url($url);

        if(isset($_SERVER['SERVER_ADDR'])){
            $ip = $_SERVER['SERVER_ADDR'];
        }else{
            $ip = '127.0.0.1';
        }

        $out = array(
            'url' => $dataurl,
            'ip' => $ip
        );

        return $out;
    }

    function missing(){
        $result = array('success' => 0,'error' => 1,'data' => array('result' => 'are u missing something?'));
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
