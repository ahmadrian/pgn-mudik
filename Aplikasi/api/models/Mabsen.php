<?php

class Mabsen extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get_absen($id){
        $sql = "SELECT 
                a.id_booking, a.id_transportasi, a.jumlah_penumpang, a.status, a.keterangan,
                b.karyawan, b.id_card,
                d.jenis,
                e.moda, e.slot,
                f.rute AS rute_awal,
                g.rute AS rute_tujuan,
                h.jadwal
                FROM t_booking AS a 
                LEFT JOIN t_karyawan AS b ON b.id_karyawan = a.id_karyawan
                LEFT JOIN t_transportasi AS c ON c.id_transportasi = a.id_transportasi
                LEFT JOIN t_jenis_moda AS d ON d.id_jenis_moda = c.id_jenis_moda
                LEFT JOIN t_moda AS e ON e.id_moda = c.id_moda
                LEFT JOIN t_rute AS f ON f.id_rute = c.rute_awal
                LEFT JOIN t_rute AS g ON g.id_rute = c.rute_tujuan
                LEFT JOIN t_jadwal AS h ON h.id_jadwal = c.id_jadwal
                WHERE a.status = 0 AND a.id_booking = ".$id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function cekabsenmasuk($id){
        $sql = "SELECT * FROM t_absen WHERE id_booking = ".$id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function check_in($id_booking){
        $date_in = date('Y-m-d H:i:s');
        $sql = "INSERT INTO t_absen (id_booking, check_in) VALUES('". $id_booking ."', '". $date_in ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }
}