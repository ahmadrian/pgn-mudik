<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>SILAHKAN LENGKAPI FORM <?php echo strtoupper($this->router->fetch_class());?><small><code>* field wajib di isi</code></small></h2>
                    </div>
                    <div class="body">
                        <form id="form_validation" method="POST" action="<?php echo base_url().$this->router->fetch_class()?>/add">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <label class="form-label">Jenis <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="id_jenis" class="form-control show-tick" data-live-search="true">
                                                <?php if(!empty($list_jenis)): ?>
                                                    <?php for($i=0; $i<count($list_jenis); $i++):?>
                                                        <option value="<?php echo $list_jenis[$i]['id_jenis_moda'];?>" ><?php echo $list_jenis[$i]['jenis'];?></option>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Moda <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="moda" required>
                                        </div>
                                    </div>
                                    <label class="form-label">Slot <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" class="form-control" name="slot" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <a href="<?php echo base_url().$this->router->fetch_class()?>" class="btn bg-grey waves-effect">KEMBALI</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn bg-pgn waves-effect" type="submit">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>