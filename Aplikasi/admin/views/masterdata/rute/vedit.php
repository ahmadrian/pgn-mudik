<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>SILAHKAN LENGKAPI FORM <?php echo strtoupper($this->router->fetch_class());?><small><code>* field wajib di isi</code></small></h2>
                    </div>
                    <div class="body">
                    <form id="form_validation" method="POST" action="<?php echo base_url().$this->router->fetch_class()."/edit/".$list[0]['id_rute']?>">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <label class="form-label">Jenis <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="rute" value="<?php echo $list[0]['rute'];?>" required>
                                        </div>
                                    </div>
                                    <label class="form-label">Status <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="status" class="form-control show-tick" data-live-search="true">
                                                <option value="0" <?php if($list[0]['status'] == 0){echo 'selected';}?> >Aktif</option>
                                                <option value="1" <?php if($list[0]['status'] == 1){echo 'selected';}?> >Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <a href="<?php echo base_url().$this->router->fetch_class()?>" class="btn bg-grey waves-effect">KEMBALI</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn bg-pgn waves-effect" type="submit">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>