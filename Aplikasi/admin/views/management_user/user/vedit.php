<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>SILAHKAN LENGKAPI FORM <?php echo strtoupper($this->router->fetch_class());?><small><code>* field wajib di isi</code></small></h2>
                    </div>
                    <div class="body">
                        <form id="form_validation" method="POST" action="<?php echo base_url().$this->router->fetch_class()."/edit/".$list[0]['id_user']?>">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <label class="form-label">Role <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="id_role" class="form-control show-tick" data-live-search="true">
                                                <?php if(!empty($list_role)): ?>
                                                    <?php for($i=0; $i<count($list_role); $i++):
                                                        $selected = '';
                                                        if($list_role[$i]['id_role'] == $list[0]['id_role']){ $selected = 'selected';}
                                                        ?>
                                                        <option value="<?php echo $list_role[$i]['id_role'];?>" <?php echo $selected;?>><?php echo $list_role[$i]['rolename'];?></option>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Username <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" value="<?php echo $list[0]['username'];?>" required>
                                        </div>
                                    </div>
                                    <label class="form-label">Fullname <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="fullname" value="<?php echo $list[0]['fullname'];?>" required>
                                        </div>
                                    </div>
                                    <label class="form-label">Status <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="status" class="form-control show-tick" data-live-search="true">
                                                <option value="0" <?php if($list[0]['status'] == 0){echo 'selected';}?> >Aktif</option>
                                                <option value="1" <?php if($list[0]['status'] == 1){echo 'selected';}?> >Block</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <a href="<?php echo base_url().$this->router->fetch_class()?>" class="btn bg-grey waves-effect">KEMBALI</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn bg-pgn waves-effect" type="submit">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>