<section class="content">
    <div class="container-fluid">
        <div class="row clearfix" id="list_data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>
                            Data <?php echo ucfirst($this->router->fetch_class());?>
                        </h2>
                        <div style="position: absolute; right: 20px; top: 10px;">
                            <a href="<?php echo base_url().$this->router->fetch_class()?>/add" class="btn bg-pgn waves-effect" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah <?php echo ucfirst($this->router->fetch_class());?>">
                                <i class="material-icons">library_add</i>
                            </a>
                        </div>
                    </div>
                    <div class="body table-responsive">

                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center" width="10%">ID</th>
                                <th>Rolename</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($list)): ?>
                                <?php for($i=0; $i<count($list); $i++): ?>
                                    <tr>
                                        <th scope="row" class="text-center"><?php echo $list[$i]['id_role']; ?></th>
                                        <td>
                                            <b><?php echo $list[$i]['rolename']?></b><br>
                                        </td>
                                        <td class="text-center">
                                            <a href="<?php echo base_url().$this->router->fetch_class().'/edit/'.$list[$i]['id_role']?>" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit <?php echo $list[$i]['rolename']; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr><td colspan="3" class="text-center">Data Kosong</td></tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <nav class=" text-right">
                            <ul class="pagination">
                                <?php echo $create_paging;?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>