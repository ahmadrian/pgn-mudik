<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>SILAHKAN LENGKAPI FORM <?php echo strtoupper($this->router->fetch_class());?><small><code>* field wajib di isi</code></small></h2>
                    </div>
                    <div class="body">
                        <form id="form_validation" method="POST" action="<?php echo base_url().$this->router->fetch_class()?>/add">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <label class="form-label">Jenis <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select id="jenis" name="id_jenis_moda" class="form-control select2" required>
                                                <option value="">== Pilih Jenis ==</option>
                                                <?php if(!empty($list_jenis)): ?>
                                                    <?php for($i=0; $i<count($list_jenis); $i++):?>
                                                        <option value="<?php echo $list_jenis[$i]['id_jenis_moda'];?>" ><?php echo $list_jenis[$i]['jenis'];?></option>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Moda<code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select id="moda" name="id_moda" class="form-control select2" required>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Rute Awal <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="rute_awal" class="form-control select2">
                                                <?php if(!empty($list_rute)): ?>
                                                    <?php for($i=0; $i<count($list_rute); $i++):?>
                                                        <option value="<?php echo $list_rute[$i]['id_rute'];?>" ><?php echo $list_rute[$i]['rute'];?></option>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Rute Tujuan <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="rute_tujuan" class="form-control select2">
                                                <?php if(!empty($list_rute)): ?>
                                                    <?php for($i=0; $i<count($list_rute); $i++):?>
                                                        <option value="<?php echo $list_rute[$i]['id_rute'];?>" ><?php echo $list_rute[$i]['rute'];?></option>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Jadwal <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="id_jadwal" class="form-control select2">
                                                <?php if(!empty($list_jadwal)): ?>
                                                    <?php for($i=0; $i<count($list_jadwal); $i++):?>
                                                        <option value="<?php echo $list_jadwal[$i]['id_jadwal'];?>" ><?php echo $list_jadwal[$i]['jadwal'];?></option>
                                                    <?php endfor; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <label class="form-label">Content</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="txtContent" class="tinyContent" name="content"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <a href="<?php echo base_url().$this->router->fetch_class()?>" class="btn bg-grey waves-effect">KEMBALI</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn bg-pgn waves-effect" type="submit">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var base_url = "<?php echo base_url()?>";
    var tinymce_uri = "<?php echo $this->config->item('template_uri');?>";

    $(function(){
        $('.select2').select2();

        $("#jenis").change(function(){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url().$this->router->fetch_class().'/show_moda'; ?>",
                data: {id_jenis : $("#jenis").val()},
                dataType: "json",
                beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response){
                    $("#moda").html(response.list_moda);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#moda").html('');
                }
            });
        });
    });

    tinymce.init({
        selector: ".tinyContent",
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : '',
        theme: "modern",
        height: 400,
        menubar: '',
        plugins: [
            "autolink link image preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks insertdatetime media nonbreaking",
            "table contextmenu directionality paste responsivefilemanager code"
        ],
        toolbar1: "bold | italic | underline | table | preview code",
        content_style: ".mce-content-body {font-size:16px;font-family:Arial,sans-serif;}",
        image_advtab: true,

        filemanager_title:"PGN Filemanager" ,
        external_filemanager_path:tinymce_uri+"/plugins/tinymce/filemanager/",
        external_plugins: { "filemanager" : tinymce_uri+"/plugins/tinymce/filemanager/plugin.min.js"}
    });
</script>