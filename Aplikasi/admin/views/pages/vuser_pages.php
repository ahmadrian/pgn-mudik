<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="theme-color" content="#02AEF0">
    <title><?php echo !empty($html['title'])?$html['title']:''; ?></title>
    <link rel="icon" href="<?php echo $this->config->item('template_uri'); ?>favicon.ico" type="image/x-icon">
    <?php if(isset($html['css'])): echo $html['css']; endif;?>
    <?php if(isset($template['newcss'])): echo $template['newcss']; endif;?>
    <?php if(isset($html['js_head'])): echo $html['js_head']; endif;?>
</head>
<body class="theme-pgn">
<?php if(isset($template['header'])): echo $template['header']; endif;?>
<section>
    <?php if(isset($template['sidebarleft'])): echo $template['sidebarleft']; endif;?>
    <?php if(isset($template['sidebarright'])): echo $template['sidebarright']; endif;?>
</section>
<?php if(isset($content['content'])): echo $content['content']; endif;?>
<?php if(isset($html['js_content'])): echo $html['js_content']; endif;?>
<script>
    $(function () {
        $('.js-sweetalert button').on('click', function () {
            var data_delete = $(this).attr("data-delete");
            var uri = $(this).attr("uri-delete");
            showCancelMessage(data_delete,uri);
        });
    });

    function showCancelMessage(data,link) {
        swal({
            title: "Anda yakin ingin menghapus data ini.?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, hapus data ini",
            cancelButtonText: "Batalkan",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                swal({
                    title: "Data berhasil dihapus",
                    text: "",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: false
                });
                $.ajax({
                    url: link+"/delete",
                    data: 'id=' + data,
                    type: 'POST',
                    success: function(msg) {
                        if(msg == "success"){
                            location.href = link;
                        }
                    }
                })
            } else {
                swal({
                    title: "Hapus dibatalkan",
                    text: "",
                    type: "error",
                    timer: 1500,
                    showConfirmButton: false
                });
            }
        });
    }
</script>
</body>
</html>