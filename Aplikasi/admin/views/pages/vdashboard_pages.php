<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="theme-color" content="#02AEF0">
    <title><?php echo !empty($html['title'])?$html['title']:''; ?></title>
    <link rel="icon" href="<?php echo $this->config->item('template_uri'); ?>favicon.ico" type="image/x-icon">
    <?php if(isset($html['css'])): echo $html['css']; endif;?>
    <?php if(isset($template['newcss'])): echo $template['newcss']; endif;?>
    <?php if(isset($html['js_head'])): echo $html['js_head']; endif;?>
</head>
<body class="theme-pgn">
<?php if(isset($template['header'])): echo $template['header']; endif;?>
<section>
    <?php if(isset($template['sidebarleft'])): echo $template['sidebarleft']; endif;?>
    <?php if(isset($template['sidebarright'])): echo $template['sidebarright']; endif;?>
</section>
<?php if(isset($content['content'])): echo $content['content']; endif;?>
<?php if(isset($html['js_content'])): echo $html['js_content']; endif;?>
<script>
    $(function() {
        setTimeout(function(){ $.ajax({url:'<?php echo site_url('dashboard/jumlahdata'); ?>',method:"GET",success:function(result){$('#jumlah-data').html(result);}}); }, 1000);
        setTimeout(function(){ $.ajax({url:'<?php echo site_url('dashboard/jumlahpenumpang'); ?>',method:"GET",success:function(result){$('#jumlah-penumpang').html(result);}}); }, 2000);

        var myEl = document.getElementById('refresh-jumlahdata');
        myEl.addEventListener('click', function() {
            $("#jumlah-data").html("<div class='col-md-12 text-center'><div class='preloader'><div class='spinner-layer pl-indigo'><div class='circle-clipper left'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div><br><div class='font-16'>Loading..</div></div>");
            setTimeout(function(){ $.ajax({url:'<?php echo site_url('dashboard/jumlahdata'); ?>',method:"GET",success:function(result){$('#jumlah-data').html(result);}}); }, 500);
        }, false);

        var myPen = document.getElementById('refresh-jumlahpenumpang')
        myPen.addEventListener('click', function() {
            $("#jumlah-penumpang").html("<div class='col-md-12 text-center'><div class='preloader'><div class='spinner-layer pl-indigo'><div class='circle-clipper left'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div><br><div class='font-16'>Loading..</div></div>");
            setTimeout(function(){ $.ajax({url:'<?php echo site_url('dashboard/jumlahpenumpang'); ?>',method:"GET",success:function(result){$('#jumlah-penumpang').html(result);}}); }, 500);
        }, false);
    });
</script>
</body>
</html>