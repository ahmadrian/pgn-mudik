<div class="animated zoomIn">
    <div class="col-md-4">
        <a href="javascript:" target="_blank" class="btn bg-bv btn-lg btn-block">Booking verifikasi
            <span class="badge bg-white col-black"><?php echo $booking_acc ?></span></a>
    </div>
    <div class="col-md-4">
        <a href="javascript:" target="_blank" class="btn bg-bd btn-lg btn-block">Booking Ditolak
            <span class="badge bg-white col-black"><?php echo $booking_reject ?></span></a>
    </div>
    <div class="col-md-4">
        <a href="javascript:" target="_blank" class="btn bg-bkg btn-lg btn-block">Booking Proses
            <span class="badge bg-white col-black"><?php echo $booking_order ?></span></a>
    </div>
    <div class="col-md-4">
        <a href="javascript:" target="_blank" class="btn bg-trs btn-lg btn-block">Jumlah Transportasi
            <span class="badge bg-white col-black"><?php echo $transportasi ?></span></a>
    </div>
    <div class="col-md-4">
        <a href="javascript:" target="_blank" class="btn bg-bk btn-lg btn-block">Jumlah Karyawan
            <span class="badge bg-white col-black"><?php echo $karyawan ?></span></a>
    </div>
</div>