<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="card">
                    <div class="header">
                        <h4 class="col-red">Jumlah Data
                        </h4>
                        <ul class="header-dropdown m-r-10">
                            <li>
                                <a id="refresh-jumlahdata" class="btn bg-indigo" data-toggle="cardloading" data-loading-effect="timer">
                                    <i class="material-icons">loop</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div id="jumlah-data">
                                <div class="col-md-12 text-center">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-indigo">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="font-16">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="header">
                        <h4 class="col-red">Jumlah Penumpang
                        </h4>
                        <ul class="header-dropdown m-r-10">
                            <li>
                                <a id="refresh-jumlahpenumpang" class="btn bg-indigo" data-toggle="cardloading" data-loading-effect="timer">
                                    <i class="material-icons">loop</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div id="jumlah-penumpang">
                                <div class="col-md-12 text-center">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-indigo">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="font-16">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>