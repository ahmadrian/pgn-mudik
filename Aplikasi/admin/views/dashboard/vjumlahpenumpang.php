<table class="table table-bordered table-hover animated slideInUp">
    <thead>
    <tr>
        <th>Info Transportasi</th>
        <th width="15%" class="text-center">SLOT</th>
        <th width="15%" class="text-center">Jumlah Penumpang</th>
        <th width="15%" class="text-center">Status</th>
    </tr>
    </thead>
    <tbody>
    <?php if(!empty($list)): ?>
        <?php for($i=0; $i<count($list); $i++):
            $status[$i] = $list[$i]['status'];
            if($status[$i] == 0){
                $show_status[$i] = "<h5><span class='label bg-green'>Kapasitas Tersedia</span></h5>";
            }elseif ($status[$i] == 1){
                $show_status[$i] = "<h5><span class='label bg-red'>Kapasitas Penuh</span></h5>";
            }else{
                $show_status[$i] = "<h5><span class='label bg-red'>Tidak Diketahui</span></h5>";
            }
            ?>
            <tr>
                <td>
                    Moda : <b><?php echo $list[$i]['jenis']?>/<?php echo $list[$i]['moda']?></b>
                    <br>
                    <br>
                    Rute : <?php echo $list[$i]['rute_awal']. '-' .$list[$i]['rute_tujuan'];?>
                    <br>
                    Jadwal Keberangkatan : <?php echo $list[$i]['jadwal'];?>
                </td>
                <td class="text-center"><?php echo $list[$i]['slot'];?></td>
                <td class="text-center">
                    <?php
                        $get_booking[$i] = $this->mdashboard->get_booking_by_transportasi($list[$i]['id_transportasi']);
                        if(count($get_booking[$i]) > 0){
                            $jp = 0;
                            for($j=0; $j<count($get_booking[$i]); $j++){
                                $jp += $get_booking[$i][$j]['jumlah_penumpang'];
                            }
                            echo $slot_ready = $list[$i]['slot']-$jp;
                        }else{
                            echo $slot_ready = $list[$i]['slot'];
                        }
                    ?>
                </td>
                <td class="text-center"><?php echo $show_status[$i];?></td>
            </tr>
        <?php endfor; ?>
    <?php else: ?>
        <tr><td colspan="5" class="text-center">Data Kosong</td></tr>
    <?php endif; ?>
    </tbody>
</table>