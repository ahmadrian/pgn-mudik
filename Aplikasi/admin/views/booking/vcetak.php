<section class="content">
    <div class="container-fluid">
        <div class="row clearfix" id="list_data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>
                            Nomor Tiket #<?php echo $list[0]['id_booking'];?>
                        </h2>
                        <div class="font-12 col-red">Tunjukan qrcode dibawah pada saat melakukan absen</div>
                    </div>
                    <div class="body table-responsive text-center">
                        <img clss="img-responsive" src="<?php echo $qrcode;?>" alt="<?php echo $list[0]['karyawan'];?>"/>
                        <br>
                        <a href="<?php echo base_url().$this->router->fetch_class().'/download/'.$list[0]['id_booking']?>" class="btn bg-pgn btn-block btn-lg waves-effect" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak Tiket <?php echo $list[0]['karyawan']; ?>" style="width: 50%;">
                            DOWNLOAD
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>