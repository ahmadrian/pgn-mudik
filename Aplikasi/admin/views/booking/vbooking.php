<section class="content">
    <div class="container-fluid">
        <div class="row clearfix" id="list_data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <h2>
                            Data <?php echo ucfirst($this->router->fetch_class());?>
                        </h2>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Info Karyawan</th>
                                <th class="text-center">Jumlah Tiket</th>
                                <th>Info Mudik</th>
                                <th width="15%" class="text-center">Status</th>
                                <th width="15%" class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($list)): ?>
                                <?php for($i=0; $i<count($list); $i++):
                                    $status[$i] = $list[$i]['status'];
                                    if($status[$i] == 0){
                                        $show_status[$i] = "<h5><span class='label bg-green'>Verifikasi</span></h5>";
                                    }elseif ($status[$i] == 1){
                                        $show_status[$i] = "<h5><span class='label bg-red'>Ditolak</span></h5>";
                                    }elseif ($status[$i] == 2){
                                        $show_status[$i] = "<h5><span class='label bg-purple'>Menunggu Proses</span></h5>";
                                    }else{
                                        $show_status[$i] = "<h5><span class='label bg-red'>Tidak Diketahui</span></h5>";
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <strong><?php echo $list[$i]['karyawan']?></strong>
                                            <br>
                                            ID Karyawan : <?php echo $list[$i]['id_card']?>
                                        </td>
                                        <td class="text-center"><?php echo $list[$i]['jumlah_penumpang'];?></td>
                                        <td>
                                            Moda : <b><?php echo $list[$i]['jenis']?>/<?php echo $list[$i]['moda']?></b>
                                            <br>
                                            <br>
                                            Rute : <?php echo $list[$i]['rute_awal']. '-' .$list[$i]['rute_tujuan'];?>
                                            <br>
                                            Jadwal Keberangkatan : <?php echo $list[$i]['jadwal'];?>
                                        </td>
                                        <td class="text-center">
                                            <?php echo $show_status[$i];?>
                                            <?php
                                            if($status[$i] == 1){
                                                echo '<div class="font-11">Keterangan : '.$list[$i]['keterangan'].'</div>';
                                            }
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="<?php echo base_url().$this->router->fetch_class().'/detail/'.$list[$i]['id_booking']?>" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail <?php echo $list[$i]['karyawan']; ?>">
                                                <i class="material-icons">keyboard_tab</i>
                                            </a>
                                            <?php if($list[$i]['status'] == 0):?>
                                                <a target="_blank" href="<?php echo base_url().$this->router->fetch_class().'/cetaktiket/'.$list[$i]['id_booking']?>" class="btn bg-pgn btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cetak Tiket <?php echo $list[$i]['karyawan']; ?>">
                                                    <i class="material-icons">print</i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr><td colspan="4" class="text-center">Data Kosong</td></tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <nav class="text-right">
                            <ul class="pagination">
                                <?php echo $create_paging;?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>