<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo $this->session->flashdata("msg"); ?>
                <div class="card">
                    <div class="header">
                        <a href="<?php echo base_url().$this->router->fetch_class();?>" class="btn bg-grey waves-effect" type="submit">Kembali</a>
                        <br>
                        <div>
                            <h4><?php echo $booking[0]['karyawan'];?></h4>
                            <h2>ID Karyawan : <?php echo $booking[0]['id_card'];?></h2>
                        </div>
                    </div>
                    <div class="body">
                        <h4>
                            <span class="col-red">Data Penumpang</span>
                        </h4>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="10%" class="text-center">No.</th>
                                <th>Nama Penumpang</th>
                                <th>Jenis Kelamin</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($list)): ?>
                                <?php for($i=0; $i<count($list); $i++):?>
                                    <tr>
                                        <td class="text-center"><?php echo $i+1;?></td>
                                        <td><?php echo $list[$i]['nama_penumpang']?></td>
                                        <td><?php echo $list[$i]['jenis_kelamin'];?></td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr><td colspan="4" class="text-center">Belum ada penumpang dari <?php echo $booking[0]['karyawan'];?></td></tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <?php if($booking[0]['status'] == 2): ?>
                        <form id="form_validation" method="POST" action="<?php echo base_url().$this->router->fetch_class()."/proses/".$booking[0]['id_booking']?>">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <label class="form-label">Proses <code>*</code></label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select id="status" name="status" class="form-control show-tick" data-live-search="true" onchange="checkStatus(this)">
                                                <option value="0" <?php if($booking[0]['status'] == 0){echo 'selected';}?> >Verifikasi</option>
                                                <option value="1" <?php if($booking[0]['status'] == 1){echo 'selected';}?> >Tolak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="keterangan">
                                        <label class="form-label">Keterangan</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <textarea type="text" name="keterangan" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <a href="<?php echo base_url().$this->router->fetch_class()?>" class="btn bg-grey waves-effect">KEMBALI</a>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn bg-pgn waves-effect" type="submit">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("#keterangan").hide();
    function checkStatus(sel) {
        var value = sel.value;
        if(value == 1){
            $("#keterangan").show();
        }else{
            $("#keterangan").hide();
        }
    }
</script>