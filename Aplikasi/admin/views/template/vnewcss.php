<style>
    .theme-pgn .navbar {
        background-color: #02AEF0;
    }

    .theme-pgn .navbar-brand {
        color: #fff;
    }

    .theme-pgn .navbar-brand:hover {
        color: #fff;
    }

    .theme-pgn .navbar-brand:active {
        color: #fff;
    }

    .theme-pgn .navbar-brand:focus {
        color: #fff;
    }

    .theme-pgn .nav > li > a {
        color: #fff;
    }

    .theme-pgn .nav > li > a:hover {
        background-color: transparent;
    }

    .theme-pgn .nav > li > a:focus {
        background-color: transparent;
    }

    .theme-pgn .nav .open > a {
        background-color: transparent;
    }

    .theme-pgn .nav .open > a:hover {
        background-color: transparent;
    }

    .theme-pgn .nav .open > a:focus {
        background-color: transparent;
    }

    .theme-pgn .bars {
        color: #fff;
    }

    .theme-pgn .sidebar .menu .list li.active {
        background-color: transparent;
    }

    .theme-pgn .sidebar .menu .list li.active > :first-child i, .theme-pgn .sidebar .menu .list li.active > :first-child span {
        color: #02AEF0;
    }

    .theme-pgn .sidebar .menu .list .toggled {
        background-color: transparent;
    }

    .theme-pgn .sidebar .menu .list .ml-menu {
        background-color: transparent;
    }

    .theme-pgn .sidebar .legal {
        background-color: #fff;
    }

    .theme-pgn .sidebar .legal .copyright a {
        color: #02AEF0 !important;
    }

    .col-pgn{color:#02AEF0;}
    .bg-pgn{background-color:#02AEF0; color:#FFF;}
    .btn:hover, .btn:focus, .btn.focus {
        color: #fff !important;
        text-decoration: none;
    }

    .bg-trs{background:#e40001 !important; color:#FFFFFF;}
    .bg-trs:hover{background:#e40001 !important; color:#FFFFFF;}

    .bg-bv{background:#fc7100 !important; color:#FFFFFF;}
    .bg-bv:hover{background:#fc7100 !important; color:#FFFFFF;}

    .bg-bk{background:#28ba3d !important; color:#FFFFFF;}
    .bg-bk:hover{background:#28ba3d !important; color:#FFFFFF;}

    .bg-bd{background:#42b4f0 !important; color:#FFFFFF;}
    .bg-bd:hover{background:#42b4f0 !important; color:#FFFFFF;}

    .bg-bkg{background:#8e44ad !important; color:#FFFFFF;}
    .bg-bkg:hover{background:#8e44ad !important; color:#FFFFFF;}
</style>