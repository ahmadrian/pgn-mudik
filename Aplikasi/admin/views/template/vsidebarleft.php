<?php
    $class = $this->router->fetch_class();
    $aktif_dashboard = '';
    if($class == "dashboard"){
        $aktif_dashboard = 'class="active"';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "role"){
        $aktif_dashboard = '';
        $aktif_role = 'class="active"';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "user"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = 'class="active"';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "jenismoda"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = 'class="active"';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "moda"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = 'class="active"';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "rute"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = 'class="active"';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "jadwal"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = 'class="active"';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "karyawan"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = 'class="active"';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }elseif ($class == "transportasi"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = 'class="active"';
        $aktif_booking = '';
    }elseif ($class == "booking"){
        $aktif_dashboard = '';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = 'class="active"';
    }else{
        $aktif_dashboard = 'class="active"';
        $aktif_role = '';
        $aktif_user = '';
        $aktif_jenis = '';
        $aktif_moda = '';
        $aktif_rute = '';
        $aktif_jadwal = '';
        $aktif_karyawan = '';
        $aktif_transportasi = '';
        $aktif_booking = '';
    }
?>

<aside id="leftsidebar" class="sidebar">
    <div class="menu">
        <ul class="list">
            <li class="header">ADMIN PANEL</li>
            <li <?php echo $aktif_dashboard;?>>
                <a href="<?php echo site_url('dashboard');?>" class=" toggled"><i class="material-icons col-light-blue">home</i><span>Dashboard</span></a>
            </li>
            <li <?php echo $aktif_role.$aktif_user;?>>
                <a href="javascript:" class="menu-toggle">
                    <i class="material-icons col-pgn">assignment_ind</i><span>Management User</span>
                </a>
                <ul class="ml-menu">
                    <li><a href="<?php echo site_url('role');?>">Role</a></li>
                    <li><a href="<?php echo site_url('user');?>">User</a></li>
                </ul>
            </li>
            <li class="header">MUDIK</li>
            <li <?php echo $aktif_jenis.$aktif_moda.$aktif_rute.$aktif_jadwal.$aktif_karyawan;?>>
                <a href="javascript:" class="menu-toggle">
                    <i class="material-icons col-pgn">library_books</i><span>Master Data</span>
                </a>
                <ul class="ml-menu">
                    <li><a href="<?php echo site_url('jenismoda');?>">Jenis Moda</a></li>
                    <li><a href="<?php echo site_url('moda');?>">Moda</a></li>
                    <li><a href="<?php echo site_url('rute');?>">Rute</a></li>
                    <li><a href="<?php echo site_url('jadwal');?>">Jadwal Keberangkatan</a></li>
                    <li><a href="<?php echo site_url('karyawan');?>">Karyawan</a></li>
                </ul>
            </li>
            <li <?php echo $aktif_transportasi;?>><a href="<?php echo site_url('transportasi');?>"><i class="material-icons col-pgn">traffic</i><span>Transportasi</span></a></li>
            <li <?php echo $aktif_booking;?>><a href="<?php echo site_url('booking');?>"><i class="material-icons col-pgn">border_color</i><span>List Booking</span></a></li>
            <li class="header"></li>
        </ul>
    </div>
    <div class="legal">
        <div class="copyright">
            <?php echo $this->config->item("copyright"); ?>
        </div>
        <div class="version">
            <b>Version: </b> 1.0
        </div>
        <p style="font-size: 12px;">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
    </div>
</aside>