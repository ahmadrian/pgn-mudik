<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alert{

    function alertMsg($code, $msg=""){
        switch ($code):
            case "success":
                $class = 'bg-pgn';
                break;
            case "failed":
                $class = 'bg-red';
                break;
            case "my-theme":
                $class = 'bg-pgn';
                break;
        endswitch;

        $data = "<div class='alert ".$class ." alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='Close'>
                    <span aria-hidden='true'>&times;</span></button>".$msg ."
                </div>";
        return $data;
    }

}