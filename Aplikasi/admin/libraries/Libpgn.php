<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Libpgn
{

    function pagination($total, $limit)
    {
        $CI = &get_instance();
        $CI->load->library('pagination');
        $config['base_url'] = base_url() . $CI->router->fetch_class() . "/";
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;
        $config['reuse_query_string'] = true;
        $config['enable_query_strings'] = true;
        $config['first_link'] = "<i class='material-icons'>first_page</i>";
        $config['last_link'] = "<i class='material-icons'>last_page</i>";
        $config['next_link'] = "<i class='material-icons'>chevron_right</i>";
        $config['prev_link'] = "<i class='material-icons'>chevron_left</i>";
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $CI->pagination->initialize($config);
        return $CI->pagination->create_links();
    }

    function sendMail($email, $message, $subject)
    {
        /*
         * - Lesssecure
         * - https://accounts.google.com/b/0/DisplayUnlockCaptchahttps://accounts.google.com/b/0/DisplayUnlockCaptcha
         * */
        $CI = & get_instance();

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'ariandev17@gmail.com',
            'smtp_pass' => 'arianariandev17',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'smtp_timeout' => 10000,
            'priority' => 1,
        );

        $CI->load->library('email', $config);
        $CI->email->set_mailtype('html');
        $CI->email->from('pgn@perkasa.com', 'pgn[at]perkasa[dot]com');
        $CI->email->set_newline("\r\n");

        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if($CI->email->send()){
            /*echo 'sukses';*/
        }else{
            echo $CI->email->print_debugger();exit;
        }
    }
}