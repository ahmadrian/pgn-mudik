<?php

class Mtransportasi extends CI_Model
{
    /* STATUS TRANSPORTASI
     * 0 TERSEDIA
     * 1 FULL
     * */

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($id_jenis_moda, $id_moda, $rute_awal, $rute_tujuan, $id_jadwal, $content, $status)
    {
        $sql = "INSERT INTO t_transportasi (id_jenis_moda, id_moda, rute_awal, rute_tujuan, id_jadwal, content, status) VALUES('". $id_jenis_moda ."','". $id_moda ."','". $rute_awal ."','". $rute_tujuan ."','". $id_jadwal ."','". $content ."','". $status ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($id_jenis_moda, $id_moda, $rute_awal, $rute_tujuan, $id_jadwal, $content, $status, $id_transportasi)
    {
        $sql = "UPDATE t_transportasi SET 
                id_jenis_moda = '". $id_jenis_moda ."', 
                id_moda = '". $id_moda ."', 
                rute_awal = '". $rute_awal ."', 
                rute_tujuan = '". $rute_tujuan ."', 
                id_jadwal = '". $id_jadwal ."', 
                content = '". $content ."', 
                status = '". $status ."' 
                WHERE id_transportasi = ".$id_transportasi;

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_transportasi WHERE id_transportasi = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT 
                a.id_transportasi, a.id_jenis_moda, a.id_moda, a.rute_awal, a.rute_tujuan, a.id_jadwal, a.content, a.status,
                b.jenis,
                c.moda, c.slot,
                d.rute AS rute_awal,
                e.rute AS rute_tujuan,
                f.jadwal
                FROM t_transportasi AS a 
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis_moda
                LEFT JOIN t_moda AS c ON c.id_moda = a.id_moda
                LEFT JOIN t_rute AS d ON d.id_rute = a.rute_awal
                LEFT JOIN t_rute AS e ON e.id_rute = a.rute_tujuan
                LEFT JOIN t_jadwal AS f ON f.id_jadwal = a.id_jadwal
                ORDER BY id_transportasi DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_transportasi FROM t_transportasi ORDER BY id_transportasi DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT 
                a.id_transportasi, a.id_jenis_moda, a.id_moda, a.rute_awal, a.rute_tujuan, a.id_jadwal, a.content, a.status,
                b.jenis,
                c.moda, c.slot,
                d.rute AS rute_awal,
                e.rute AS rute_tujuan,
                f.jadwal
                FROM t_transportasi AS a 
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis_moda
                LEFT JOIN t_moda AS c ON c.id_moda = a.id_moda
                LEFT JOIN t_rute AS d ON d.id_rute = a.rute_awal
                LEFT JOIN t_rute AS e ON e.id_rute = a.rute_tujuan
                LEFT JOIN t_jadwal AS f ON f.id_jadwal = a.id_jadwal 
                WHERE a.id_transportasi = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}