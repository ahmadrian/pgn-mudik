<?php

class Mbooking extends CI_Model
{
    /* STATUS TRANSPORTASI
     * 0 acc
     * 1 reject
     * 2 order
     * */

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function verifikasi($status, $keterangan, $id)
    {
        $sql = "UPDATE t_booking SET status = '". $status ."', keterangan = '". $keterangan ."' WHERE id_booking = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_transportasi WHERE id_transportasi = '" . $id . "'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_data($start = NULL, $offset = NULL)
    {
        if ($offset == NULL) {
            $limit = "";
        } else {
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT 
                a.id_booking, a.id_transportasi, a.jumlah_penumpang, a.status, a.keterangan,
                b.karyawan, b.id_card,
                d.jenis,
                e.moda, e.slot,
                f.rute AS rute_awal,
                g.rute AS rute_tujuan,
                h.jadwal
                FROM t_booking AS a 
                LEFT JOIN t_karyawan AS b ON b.id_karyawan = a.id_karyawan
                LEFT JOIN t_transportasi AS c ON c.id_transportasi = a.id_transportasi
                LEFT JOIN t_jenis_moda AS d ON d.id_jenis_moda = c.id_jenis_moda
                LEFT JOIN t_moda AS e ON e.id_moda = c.id_moda
                LEFT JOIN t_rute AS f ON f.id_rute = c.rute_awal
                LEFT JOIN t_rute AS g ON g.id_rute = c.rute_tujuan
                LEFT JOIN t_jadwal AS h ON h.id_jadwal = c.id_jadwal
                ORDER BY a.id_booking DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data()
    {
        $sql = "SELECT id_transportasi FROM t_transportasi ORDER BY id_transportasi DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_detail_booking($id)
    {
        $sql = "SELECT 
                a.id_detail_booking, a.id_booking, a.nama_penumpang, a.jenis_kelamin,
                b.status,
                c.karyawan, c.id_card
                FROM t_detail_booking AS a 
                LEFT JOIN t_booking AS b ON b.id_booking = a.id_booking
                LEFT JOIN t_karyawan AS c ON c.id_karyawan = b.id_karyawan
                WHERE a.id_booking = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id)
    {
        $sql = "SELECT 
                a.id_booking, a.id_karyawan, a.status,
                b.karyawan, b.id_card, b.email
                FROM t_booking AS a 
                LEFT JOIN t_karyawan AS b ON b.id_karyawan = a.id_karyawan
                WHERE a.id_booking = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_booking_by_transportasi($id)
    {
        $sql = "SELECT 
                id_booking, id_transportasi, jumlah_penumpang 
                FROM t_booking 
                WHERE
                (status = 0 OR status = 2 ) AND 
                id_transportasi = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}