<?php

class Muser extends CI_Model
{
    /* STATUS
     * 0 aktif
     * 1 block
     * */

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($id_role, $username, $fullname, $password, $status)
    {
        $sql = "INSERT INTO t_user (id_role, username, fullname, password, status) VALUES('". $id_role ."','". $username ."','". $fullname ."','". $password ."','". $status ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($id_role, $username, $fullname, $status, $id_user)
    {
        $sql = "UPDATE t_user 
                SET 
                id_role = '". $id_role ."', 
                username = '". $username ."', 
                fullname = '". $fullname ."', 
                status = '". $status ."' 
                WHERE id_user = '". $id_user ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update_password($password, $id_user)
    {
        $sql = "UPDATE t_user 
                SET 
                password = '". $password ."'
                WHERE id_user = '". $id_user ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_user WHERE id_user = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) as new_id from " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($username){
        $sql = "SELECT id_user FROM t_user WHERE username = '". $username ."'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($username, $id_user){
        $sql = "SELECT id_user FROM t_user WHERE username = '". $username ."' AND id_user != '". $id_user ."' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT 
                a.id_user, a.username, a.fullname, a.status, b.rolename 
                FROM t_user AS a
                LEFT JOIN t_role AS b ON b.id_role = a.id_role 
                ORDER BY a.id_user DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_user, username FROM t_user ORDER BY id_user DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT id_user, id_role, username, fullname, password, status FROM t_user WHERE id_user = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}