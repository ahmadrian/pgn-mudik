<?php

class Mrole extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($rolename)
    {
        $sql = "INSERT INTO t_role (rolename) VALUES('". $rolename ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($rolename, $id_role)
    {
        $sql = "UPDATE t_role SET rolename = '". $rolename ."' WHERE id_role = '". $id_role ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_role WHERE id_role = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) as new_id from " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($rolename){
        $sql = "SELECT id_role FROM t_role WHERE rolename = '". $rolename ."'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($rolename, $id_role){
        $sql = "SELECT id_role FROM t_role WHERE rolename = '". $rolename ."' AND id_role != '". $id_role ."' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_role, rolename FROM t_role ORDER BY id_role DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_role, rolename FROM t_role ORDER BY id_role DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT id_role, rolename FROM t_role WHERE id_role = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function mastersql($start,$offset){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "
            SELECT 
            a.id_kolom, a.title, a.url_title, a.harga, a.tipe_harga, a.summary, a.photo, a.date_created,
            b.kategori,
            c.nama_pengiklan,
            d.provinsi,
            e.kabupaten
            FROM t_kolom a
            LEFT JOIN t_kategori AS b ON b.id_kategori = a.id_kategori
            LEFT JOIN t_pengiklan AS c ON c.id_pengiklan = a.id_pengiklan
            LEFT JOIN t_provinsi AS d ON d.id_provinsi = a.id_provinsi
            LEFT JOIN t_kabupaten AS e ON e.id_kabupaten = a.id_kabupaten 
            WHERE a.status = 0 
            ORDER BY a.date_created DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}