<?php

class Mdashboard extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function num_moda()
    {
        $sql = "SELECT id_moda, moda FROM t_moda";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function num_rute()
    {
        $sql = "SELECT id_rute, rute FROM t_rute";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function num_transportasi()
    {
        $sql = "SELECT id_transportasi FROM t_transportasi";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function num_booking($status)
    {
        $sql = "SELECT id_booking FROM t_booking WHERE status = ".$status;

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function num_karyawan()
    {
        $sql = "SELECT id_karyawan FROM t_karyawan";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_booking_transportasi(){
        $sql = "SELECT 
                a.id_transportasi, a.id_jenis_moda, a.id_moda, a.rute_awal, a.rute_tujuan, a.id_jadwal, a.content, a.status,
                b.jenis,
                c.moda, c.slot,
                d.rute AS rute_awal,
                e.rute AS rute_tujuan,
                f.jadwal
                FROM t_transportasi AS a 
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis_moda
                LEFT JOIN t_moda AS c ON c.id_moda = a.id_moda
                LEFT JOIN t_rute AS d ON d.id_rute = a.rute_awal
                LEFT JOIN t_rute AS e ON e.id_rute = a.rute_tujuan
                LEFT JOIN t_jadwal AS f ON f.id_jadwal = a.id_jadwal
                ORDER BY id_transportasi DESC";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_booking_by_transportasi($id)
    {
        $sql = "SELECT 
                id_booking, id_transportasi, jumlah_penumpang 
                FROM t_booking 
                WHERE
                (status = 0 OR status = 2 ) AND 
                id_transportasi = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }
}