<?php

class Mjadwal extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($jadwal, $status)
    {
        $sql = "INSERT INTO t_jadwal (jadwal, status) VALUES('". $jadwal ."', '". $status ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($jadwal, $status, $id_jadwal)
    {
        $sql = "UPDATE t_jadwal SET jadwal = '". $jadwal ."', status = '". $status ."' WHERE id_jadwal = '". $id_jadwal ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_jadwal WHERE id_jadwal = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($jadwal){
        $sql = "SELECT id_jadwal FROM t_jadwal WHERE jadwal = '". $jadwal ."'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($jadwal, $id_jadwal){
        $sql = "SELECT id_jadwal FROM t_jadwal WHERE jadwal = '". $jadwal ."' AND id_jadwal != '". $id_jadwal ."' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_jadwal, jadwal, status FROM t_jadwal ORDER BY id_jadwal DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_jadwal, jadwal, status FROM t_jadwal ORDER BY id_jadwal DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_aktif($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_jadwal, jadwal FROM t_jadwal WHERE status = 0 ORDER BY id_jadwal DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT id_jadwal, jadwal, status FROM t_jadwal WHERE id_jadwal = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}