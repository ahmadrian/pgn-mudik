<?php

class Mrute extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($rute, $status)
    {
        $sql = "INSERT INTO t_rute (rute, status) VALUES('". $rute ."', '". $status ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($rute, $status, $id_rute)
    {
        $sql = "UPDATE t_rute SET rute = '". $rute ."', status = '". $status ."' WHERE id_rute = '". $id_rute ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_rute WHERE id_rute = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($rute){
        $sql = "SELECT id_rute FROM t_rute WHERE rute = '". $rute ."'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($rute, $id_rute){
        $sql = "SELECT id_rute FROM t_rute WHERE rute = '". $rute ."' AND id_rute != '". $id_rute ."' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_rute, rute, status FROM t_rute ORDER BY id_rute DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_rute, rute, status FROM t_rute ORDER BY id_rute DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_aktif($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_rute, rute FROM t_rute WHERE status = 0 ORDER BY id_rute DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT id_rute, rute, status FROM t_rute WHERE id_rute = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}