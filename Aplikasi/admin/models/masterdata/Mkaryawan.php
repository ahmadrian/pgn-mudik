<?php

class Mkaryawan extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($id_card, $karyawan, $divisi, $jenis_kelamin, $email)
    {
        $sql = "INSERT INTO t_karyawan (id_card, karyawan, divisi, jenis_kelamin, email) VALUES('". $id_card ."', '". $karyawan ."', '". $divisi ."', '". $jenis_kelamin ."', '". $email ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($id_card, $karyawan, $divisi, $jenis_kelamin, $email, $id_karyawan)
    {
        $sql = "UPDATE t_karyawan 
                SET 
                id_card = '". $id_card ."', 
                karyawan = '". $karyawan ."', 
                divisi = '". $divisi ."', 
                jenis_kelamin = '". $jenis_kelamin ."', 
                email = '". $email ."'
                WHERE id_karyawan = '". $id_karyawan ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_karyawan WHERE id_karyawan = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($karyawan){
        $sql = "SELECT id_karyawan FROM t_karyawan WHERE karyawan = '". $karyawan ."'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($karyawan, $id_karyawan){
        $sql = "SELECT id_karyawan FROM t_karyawan WHERE karyawan = '". $karyawan ."' AND id_karyawan != '". $id_karyawan ."' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_karyawan, id_card, karyawan, divisi, jenis_kelamin, email FROM t_karyawan ORDER BY id_karyawan DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_karyawan FROM t_karyawan ORDER BY id_karyawan DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT id_karyawan, id_card, karyawan, divisi, jenis_kelamin, email FROM t_karyawan WHERE id_karyawan = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}