<?php

class Mjenismoda extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($jenis, $status)
    {
        $sql = "INSERT INTO t_jenis_moda (jenis, status) VALUES('". $jenis ."', '". $status ."')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($jenis, $status, $id_jenis)
    {
        $sql = "UPDATE t_jenis_moda SET jenis = '". $jenis ."', status = '". $status ."' WHERE id_jenis_moda = '". $id_jenis ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_jenis_moda WHERE id_jenis_moda = '". $id ."'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0){
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($jenis){
        $sql = "SELECT id_jenis_moda FROM t_jenis_moda WHERE jenis = '". $jenis ."'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($jenis, $id_jenis){
        $sql = "SELECT id_jenis_moda FROM t_jenis_moda WHERE jenis = '". $jenis ."' AND id_jenis_moda != '". $id_jenis ."' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_jenis_moda, jenis, status FROM t_jenis_moda ORDER BY id_jenis_moda DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data(){
        $sql = "SELECT id_jenis_moda, jenis, status FROM t_jenis_moda ORDER BY id_jenis_moda DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_aktif($start = NULL,$offset = NULL){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT id_jenis_moda, jenis FROM t_jenis_moda WHERE status = 0 ORDER BY id_jenis_moda DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id){
        $sql = "SELECT id_jenis_moda, jenis, status FROM t_jenis_moda WHERE id_jenis_moda = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}