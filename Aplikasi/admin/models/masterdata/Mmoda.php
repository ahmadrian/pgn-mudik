<?php

class Mmoda extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($id_jenis, $moda, $slot, $status)
    {
        $sql = "INSERT INTO t_moda (id_jenis, moda, slot, status) VALUES('" . $id_jenis . "', '" . $moda . "', '" . $slot . "', '" . $status . "')";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function update($id_jenis, $moda, $slot, $status, $id_moda)
    {
        $sql = "UPDATE t_moda 
                SET 
                id_jenis = '" . $id_jenis . "', 
                moda = '" . $moda . "' ,
                slot = '" . $slot . "', 
                status = '" . $status . "' 
                WHERE id_moda = '" . $id_moda . "'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete($id)
    {
        $sql = "DELETE FROM t_moda WHERE id_moda = '" . $id . "'";

        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function get_new_id($table, $id)
    {
        $res = $this->db->query("SELECT max($id) AS new_id FROM " . $table);
        if ($res->num_rows >= 0) {
            $newId = $res->result_array();
        }

        $this->db->close();

        return $newId[0]['new_id'] + 1;
    }

    function check_exist($moda)
    {
        $sql = "SELECT id_moda FROM t_moda WHERE moda = '" . $moda . "'";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function check_exist_edit($moda, $id_moda)
    {
        $sql = "SELECT id_moda FROM t_moda WHERE moda = '" . $moda . "' AND id_moda != '" . $id_moda . "' ";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data($start = NULL, $offset = NULL)
    {
        if ($offset == NULL) {
            $limit = "";
        } else {
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT 
                a.id_moda, a.id_jenis, a.moda, a.slot, a.status, b.jenis
                FROM t_moda AS a
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis 
                ORDER BY a.id_moda DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data_aktif($start = NULL, $offset = NULL)
    {
        if ($offset == NULL) {
            $limit = "";
        } else {
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "SELECT 
                a.id_moda, a.id_jenis, a.moda, a.slot, a.status, b.jenis
                FROM t_moda AS a
                LEFT JOIN t_jenis_moda AS b ON b.id_jenis_moda = a.id_jenis
                WHERE a.status = 0 
                ORDER BY a.id_moda DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_num_data()
    {
        $sql = "SELECT id_moda, moda FROM t_moda ORDER BY id_moda DESC";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function get_data_by_id($id)
    {
        $sql = "SELECT id_moda, id_jenis, moda, slot, status FROM t_moda WHERE id_moda = " . $id;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function get_data_by_id_jenis($id)
    {
        $sql = "SELECT id_moda, id_jenis, moda, slot, status FROM t_moda WHERE id_jenis = " . $id ." AND status = 0";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

}