<?php
$config['header_name'] = 'PGN Perkasa - Aplikasi Mudik Gratis';
$config['copyright'] = '&copy; '. date('Y') .' <a href="http://pgn-perkasa.co.id/" target="_blank">PGN Perkasa</a>';
$config['version'] = '1.0';
$config['title_web'] = 'PGN Perkasa - Aplikasi Mudik Gratis';

$config['template_uri'] = "http://cmsmudik.ahmadrian.com/assets/2020/";

$config['images_uri_qrcode'] = 'http://mudik.ahmadrian.com/upload/qrcode/images/';
$config['upload_foto_qrcode'] = "../public/upload/qrcode/images/";