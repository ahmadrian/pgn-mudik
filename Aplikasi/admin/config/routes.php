<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['logout'] = 'login/logout';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* PAGING */
$route['role/(:num)'] = "role";
$route['user/(:num)'] = "user";
$route['jenismoda/(:num)'] = "jenismoda";
$route['moda/(:num)'] = "moda";
$route['rute/(:num)'] = "rute";
$route['jadwal/(:num)'] = "jadwal";
$route['transportasi/(:num)'] = "transportasi";
$route['booking/(:num)'] = "booking";