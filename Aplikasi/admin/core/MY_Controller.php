<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->check_login();
    }

    function check_login(){
        $CI =& get_instance();
        $login = $CI->session->userdata('user_data');
        if(empty($login)){
            redirect(base_url());
        }
    }
}
