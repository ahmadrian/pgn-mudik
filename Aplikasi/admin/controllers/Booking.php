<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends MY_Controller
{
    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->load->library("libpgn");
        $this->load->library("alert");
        $this->load->library('ciqrcode');
        $this->load->model('booking/mbooking');
    }

    function index()
    {
        $a['html']['title'] = 'Booking ' . $this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $start = 0;
        if ($this->uri->segment(2)) {
            $start = $this->uri->segment(2);
        }

        $t['list'] = $this->mbooking->get_data($start, $this->limit);
        $t['totalData'] = $this->mbooking->get_num_data();

        $create_paging = '';
        if ($t['totalData'] > 0) {
            $create_paging = $this->libpgn->pagination($t['totalData'], $this->limit);
        }
        $t['create_paging'] = $create_paging;

        $a['content']['content'] = $this->load->view('booking/vbooking', $t, TRUE);

        $this->load->view('pages/vbooking_pages', $a, FALSE);
    }

    function detail()
    {
        $id = $this->uri->segment(3);
        if (empty($id)) {
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Uri tidak diketahui"));
            redirect(base_url() . $this->router->fetch_class());
        }

        $a['html']['title'] = 'Detail Booking ' . $this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $get_detail = $this->mbooking->get_detail_booking($id);
        $t['list'] = $get_detail;

        $t['booking'] = $this->mbooking->get_data_by_id($id);

        $a['content']['content'] = $this->load->view('booking/vdetail', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vbooking_pages', $a, FALSE);
    }

    function proses()
    {
        if($this->input->post()){
            $id = $this->uri->segment(3);
            $get_karyawan = $this->mbooking->get_data_by_id($id);

            if($this->input->post('status') == 0){
                $qrcodeUrl = 'http://mudik.ahmadrian.com/absen/'.$id;
                $image_name = $id.'.jpg';

                $params['data'] = $qrcodeUrl;
                $params['level'] = 'H';
                $params['size'] = 10;
                $params['savename'] = $this->config->item('upload_foto_qrcode').$image_name;
                $this->ciqrcode->generate($params);
                $qrcode = $this->config->item('images_uri_qrcode').$image_name;

                $email = 'ahmadrianfadli@gmail.com';
                $subject = 'Mudik bersama PGN Perkasa';
                $fullname = $get_karyawan[0]['karyawan'];
                $pesan = array(
                    'logo' => "http://cmsmudik.ahmadrian.com/assets/2020/favicon.ico",
                    'AE_ADMIN' => '<p>Terima Kasih<br><br>Salam, <br>Admin<br>PT Permata Karya Jasa</p>',

                    'username' => $fullname,
                    'pesan'     => 'Pengajuan mudik bersama pgn terverifikasi<br>Silahkan perlihatkan email ini ke panitia sebagai tanda absen Anda :',
                    'images' => $qrcode,
                );
                $message = $this->load->view('template/vtemplate_email',$pesan, TRUE);
//                $this->libpgn->sendMail($email,$message,$subject);

                $this->session->set_flashdata("msg", $this->alert->alertMsg("success", "Qrcode dikirim ke email ".$get_karyawan[0]['email']));
            }else{
                $this->session->set_flashdata("msg", $this->alert->alertMsg("success", "Update berhasil"));
            }

            $this->mbooking->verifikasi($this->input->post('status'),$this->input->post('keterangan'), $id);
            redirect(base_url() . $this->router->fetch_class());
        }
    }

    function cetaktiket(){
        $id = $this->uri->segment(3);
        if (empty($id)) {
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Uri tidak diketahui"));
            redirect(base_url() . $this->router->fetch_class());
        }

        $a['html']['title'] = 'Detail Booking ' . $this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $get_detail = $this->mbooking->get_data_by_id($id);
        $t['list'] = $get_detail;

        $qrcodeUrl = 'http://mudik.ahmadrian.com/absen/'.$get_detail[0]['id_booking'];
        $image_name = $get_detail[0]['id_booking'].'.jpg';

        $params['data'] = $qrcodeUrl;
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = $this->config->item('upload_foto_qrcode').$image_name;
        $this->ciqrcode->generate($params);
        $t['qrcode'] = $this->config->item('images_uri_qrcode').$image_name;

        $a['content']['content'] = $this->load->view('booking/vcetak', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vbooking_pages', $a, FALSE);
    }

    function download()
    {
        $id = $this->uri->segment(3);
        if (empty($id)) {
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Uri tidak diketahui"));
            redirect(base_url() . $this->router->fetch_class());
        }
        $path = $this->config->item('upload_foto_qrcode').$id.'.jpg';
        force_download($path, NULL);
    }

}