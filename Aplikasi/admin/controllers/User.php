<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->library("libpgn");
        $this->load->library("alert");
        $this->load->model('management_user/mrole');
        $this->load->model('management_user/muser');
    }

    function index()
    {
        $a['html']['title'] = 'User '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/sweetalert/sweetalert.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');
        $a['html']['js_content'] .= add_js('plugins/sweetalert/sweetalert.min.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $start = 0;
        if($this->uri->segment(2)){
            $start = $this->uri->segment(2);
        }

        $t['list'] = $this->muser->get_data($start, $this->limit);
        $t['totalData'] = $this->muser->get_num_data();

        $create_paging = '';
        if($t['totalData'] > 0){
            $create_paging = $this->libpgn->pagination($t['totalData'],$this->limit);
        }
        $t['create_paging'] = $create_paging;

        $a['content']['content'] = $this->load->view('management_user/user/vuser', $t, TRUE);

        $this->load->view('pages/vuser_pages', $a, FALSE);
    }

    function add(){
        if($this->input->post()){
            $msg = '';
            if($this->input->post('id_role') == NULL){ $msg .= "<p>Role kosong</p>"; }
            if($this->input->post('username') == NULL){ $msg .= "<p>Username kosong</p>"; }
            if($this->input->post('fullname') == NULL){ $msg .= "<p>Fullname kosong</p>"; }
            if($this->input->post('password') == NULL){ $msg .= "<p>Password kosong</p>"; }
            if($this->input->post('id_role') == NULL || $this->input->post('username') == NULL || $this->input->post('fullname') == NULL || $this->input->post('password') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class());
            }

            /*check apakah user sudah ada*/
            $check = $this->muser->check_exist($this->input->post('username'));
            if($check > 0){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Username <strong>".$this->input->post('username')."</strong> sudah ada"));
                redirect(base_url().$this->router->fetch_class());
            }

            $id_role = $this->input->post('id_role');
            $username = addslashes(trim($this->input->post('username')));
            $fullname = addslashes(trim($this->input->post('fullname')));
            $password = trim(md5(sha1($this->input->post('password'))));
            $status = 0;
            $this->muser->insert($id_role,$username,$fullname,$password,$status);

            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Data berhasil tersimpan"));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Tambah Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $t['list_role'] = $this->mrole->get_data();

        $a['content']['content'] = $this->load->view('management_user/user/vadd', $t, TRUE);

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vuser_pages', $a, FALSE);
    }

    function edit(){
        $id = $this->uri->segment(3);
        if(empty($id)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Uri tidak diketahui"));
            redirect(base_url().$this->router->fetch_class());
        }

        if($this->input->post()){
            $msg = '';
            if($this->input->post('id_role') == NULL){ $msg .= "<p>Role kosong</p>"; }
            if($this->input->post('username') == NULL){ $msg .= "<p>Username kosong</p>"; }
            if($this->input->post('fullname') == NULL){ $msg .= "<p>Fullname kosong</p>"; }
            if($this->input->post('id_role') == NULL || $this->input->post('username') == NULL || $this->input->post('fullname') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class()."/edit/".$id);
            }

            /*cek username sudah ada atau belum*/
            $check_username = $this->muser->check_exist_edit($this->input->post('username'), $id);
            if(!empty($check_username)){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Username <strong>".$this->input->post('username')."</strong> sudah ada yang gunakan"));
                redirect(base_url().$this->router->fetch_class()."/edit/".$id);
            }

            $id_role = $this->input->post('id_role');
            $username = addslashes(trim($this->input->post('username')));
            $fullname = addslashes(trim($this->input->post('fullname')));
            $status = $this->input->post('status');
            $this->muser->update($id_role, $username, $fullname, $status, $id);

            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Update berhasil ".$msg));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Edit Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $get_detail = $this->muser->get_data_by_id($id);
        if(empty($get_detail)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Data tidak ada dalam database"));
            redirect(base_url().$this->router->fetch_class());
        }
        $t['list'] = $get_detail;

        $t['list_role'] = $this->mrole->get_data();

        $a['content']['content'] = $this->load->view('management_user/user/vedit', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vuser_pages', $a, FALSE);
    }

    function changepassword(){
        $id = $this->uri->segment(3);
        if(empty($id)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Uri tidak diketahui"));
            redirect(base_url().$this->router->fetch_class());
        }

        if($this->input->post()){
            if(isset($_POST['password'])){
                $password = trim(md5(sha1($this->input->post('password'))));
                $this->muser->update_password($password, $id);
                $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Update berhasil"));
            }

            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Ganti Password Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $get_detail = $this->muser->get_data_by_id($id);
        if(empty($get_detail)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Data tidak ada dalam database"));
            redirect(base_url().$this->router->fetch_class());
        }
        $t['list'] = $get_detail;

        $a['content']['content'] = $this->load->view('management_user/user/vgantipassword', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vuser_pages', $a, FALSE);
    }

    function delete()
    {
        if ($this->input->post()) {
            $get_detail = $this->muser->get_data_by_id($this->input->post('id'));
            if (!empty($get_detail)) {
                $this->muser->delete($this->input->post('id'));
                echo "success";
            }
        }
    }

}