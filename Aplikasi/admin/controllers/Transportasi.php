<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transportasi extends MY_Controller
{
    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->library("libpgn");
        $this->load->library("alert");
        $this->load->model('transportasi/mtransportasi');
        $this->load->model('masterdata/mjenismoda');
        $this->load->model('masterdata/mmoda');
        $this->load->model('masterdata/mrute');
        $this->load->model('masterdata/mjadwal');
        $this->load->model('booking/mbooking');
    }

    function index()
    {
        $a['html']['title'] = 'Transportasi '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $start = 0;
        if($this->uri->segment(2)){
            $start = $this->uri->segment(2);
        }

        $t['list'] = $this->mtransportasi->get_data($start, $this->limit);
        $t['totalData'] = $this->mtransportasi->get_num_data();

        $create_paging = '';
        if($t['totalData'] > 0){
            $create_paging = $this->libpgn->pagination($t['totalData'],$this->limit);
        }
        $t['create_paging'] = $create_paging;

        $a['content']['content'] = $this->load->view('transportasi/vtransportasi', $t, TRUE);

        $this->load->view('pages/vtransportasi_pages', $a, FALSE);
    }

    function add(){
        if($this->input->post()){
            $msg = '';
            if($this->input->post('id_jenis_moda') == NULL){ $msg .= "<p>Jenis kosong</p>"; }
            if($this->input->post('id_moda') == NULL){ $msg .= "<p>Moda kosong</p>"; }
            if($this->input->post('rute_awal') == NULL){ $msg .= "<p>Rute Awal kosong</p>"; }
            if($this->input->post('rute_tujuan') == NULL){ $msg .= "<p>Rute Tujuan kosong</p>"; }
            if($this->input->post('id_jadwal') == NULL){ $msg .= "<p>Jadwal kosong</p>"; }
            if($this->input->post('id_jenis_moda') == NULL || $this->input->post('id_moda') == NULL || $this->input->post('rute_awal') == NULL || $this->input->post('rute_tujuan') == NULL || $this->input->post('id_jadwal') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class());
            }

            $id_jenis_moda = $this->input->post('id_jenis_moda');
            $id_moda = $this->input->post('id_moda');
            $rute_awal = $this->input->post('rute_awal');
            $rute_tujuan = $this->input->post('rute_tujuan');
            $id_jadwal = $this->input->post('id_jadwal');
            $_POST['content'] = addslashes($this->input->post('content'));
            $_POST['content'] = str_replace("<p>", "", $this->input->post('content'));
            $_POST['content'] = str_replace("</p>", "", $this->input->post('content'));
            $_POST['content'] = str_replace("<script>", "", $this->input->post('content'));
            $_POST['content'] = str_replace("</script>", "", $this->input->post('content'));
            $content = $this->input->post('content');
            $status = 0;

            $this->mtransportasi->insert($id_jenis_moda,$id_moda,$rute_awal,$rute_tujuan,$id_jadwal,$content,$status);
            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Data berhasil tersimpan"));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Tambah Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/select2/select2.min.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');
        $a['html']['js_head'] .= add_js('plugins/tinymce/tinymce/tinymce.min.js');
        $a['html']['js_head'] .= add_js('plugins/tinymce/filemanager/plugin.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/select2/select2.min.js');

        $t['list_jenis'] = $this->mjenismoda->get_data_aktif();
        $t['list_moda'] = $this->mmoda->get_data_aktif();
        $t['list_rute'] = $this->mrute->get_data_aktif();
        $t['list_jadwal'] = $this->mjadwal->get_data_aktif();

        $a['content']['content'] = $this->load->view('transportasi/vadd', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vtransportasi_pages', $a, FALSE);
    }

    function edit(){
        $id = $this->uri->segment(3);
        if(empty($id)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Uri tidak diketahui"));
            redirect(base_url().$this->router->fetch_class());
        }

        if($this->input->post()){
            $msg = '';
            if($this->input->post('id_jenis_moda') == NULL){ $msg .= "<p>Jenis kosong</p>"; }
            if($this->input->post('id_moda') == NULL){ $msg .= "<p>Moda kosong</p>"; }
            if($this->input->post('rute_awal') == NULL){ $msg .= "<p>Rute Awal kosong</p>"; }
            if($this->input->post('rute_tujuan') == NULL){ $msg .= "<p>Rute Tujuan kosong</p>"; }
            if($this->input->post('id_jadwal') == NULL){ $msg .= "<p>Jadwal kosong</p>"; }
            if($this->input->post('id_jenis_moda') == NULL || $this->input->post('id_moda') == NULL || $this->input->post('rute_awal') == NULL || $this->input->post('rute_tujuan') == NULL || $this->input->post('id_jadwal') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class().'/edit/'.$id);
            }

            $id_jenis_moda = $this->input->post('id_jenis_moda');
            $id_moda = $this->input->post('id_moda');
            $rute_awal = $this->input->post('rute_awal');
            $rute_tujuan = $this->input->post('rute_tujuan');
            $id_jadwal = $this->input->post('id_jadwal');
            $_POST['content'] = addslashes($this->input->post('content'));
            $_POST['content'] = str_replace("<p>", "", $this->input->post('content'));
            $_POST['content'] = str_replace("</p>", "", $this->input->post('content'));
            $_POST['content'] = str_replace("<script>", "", $this->input->post('content'));
            $_POST['content'] = str_replace("</script>", "", $this->input->post('content'));
            $content = $this->input->post('content');
            $status = $this->input->post('status');

            $this->mtransportasi->update($id_jenis_moda,$id_moda,$rute_awal,$rute_tujuan,$id_jadwal,$content,$status, $id);
            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Update berhasil ".$msg));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Edit Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/select2/select2.min.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');
        $a['html']['js_head'] .= add_js('plugins/tinymce/tinymce/tinymce.min.js');
        $a['html']['js_head'] .= add_js('plugins/tinymce/filemanager/plugin.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/select2/select2.min.js');

        $get_detail = $this->mtransportasi->get_data_by_id($id);
        if(empty($get_detail)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Data tidak ada dalam database"));
            redirect(base_url().$this->router->fetch_class());
        }
        $t['list'] = $get_detail;

        $t['list_jenis'] = $this->mjenismoda->get_data_aktif();
        $t['list_moda'] = $this->mmoda->get_data_aktif();
        $t['list_rute'] = $this->mrute->get_data_aktif();
        $t['list_jadwal'] = $this->mjadwal->get_data_aktif();

        $a['content']['content'] = $this->load->view('transportasi/vedit', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vtransportasi_pages', $a, FALSE);
    }

    function show_moda(){
        if(!empty($this->input->post('id_jenis'))) {
            $id_jenis = $this->input->post('id_jenis');
            $moda = $this->mmoda->get_data_by_id_jenis($id_jenis);

            $lists = '';
            for ($i = 0; $i < count($moda); $i++) {
                $lists .= '<option value="' . $moda[$i]['id_moda'] . '">' . $moda[$i]['moda'] . '</option>';
            }

            $callback = array('list_moda' => $lists);
            echo json_encode($callback);
        }
    }

}