<?php

class Login extends CI_Controller
{

    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->library("alert");
        $this->load->model("mlogin");
    }

    function index()
    {
        $a['html']['title'] = $this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] = add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');

        $this->load->view('login/vlogin', $a, FALSE);
    }

    function process()
    {
        if (isset($_POST['g-recaptcha-response'])) {
            $secret = '6LdYY7IZAAAAALina_38WpcyKWVqoGAyWCqVL99K';
            $response = $_POST["g-recaptcha-response"];
            $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            $captcha_success = json_decode($verify);

            if ($captcha_success->success == true) {
                $username = addslashes(trim($this->input->post('uname')));
                $password = md5(sha1(trim($this->input->post('pwd'))));
                $check_login = $this->mlogin->get_data($username, $password);

                if (!empty($check_login)) {
                    /*user diblock*/
                    if ($check_login[0]['status'] == '1') {
                        $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "maaf anda diblock, silahkan hubungi TI PGN Perkasa"));
                        redirect(base_url() . $this->router->fetch_class());
                    }

                    $user_session = array(
                        'id_user' => $check_login[0]['id_user'],
                        'username' => $check_login[0]['username'],
                        'fullname' => $check_login[0]['fullname'],
                        'status' => $check_login[0]['status'],
                        'id_role' => $check_login[0]['id_role'],
                        'rolename' => $check_login[0]['rolename'],
                    );
                    $this->session->set_userdata("user_data", $user_session);

                    $this->session->set_flashdata("msg", $this->alert->alertMsg("success", "Selamat Datang " . ucwords($check_login[0]['fullname'])));
                    redirect(base_url("dashboard"));
                } else {
                    $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Username atau Password salah"));
                    redirect(base_url());
                }

            } else {
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Captcha tidak sesuai"));
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed", "Captcha tidak ditemukan"));
            redirect(base_url());
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect();
    }
}