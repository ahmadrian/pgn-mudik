<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Moda extends MY_Controller
{
    var $limit = 10;

    function __construct()
    {
        parent::__construct();
        $this->load->library("libpgn");
        $this->load->library("alert");
        $this->load->model('masterdata/mmoda');
        $this->load->model('masterdata/mjenismoda');
    }

    function index()
    {
        $a['html']['title'] = 'Moda '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('js/tooltips-popovers.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $start = 0;
        if($this->uri->segment(2)){
            $start = $this->uri->segment(2);
        }

        $t['list'] = $this->mmoda->get_data($start, $this->limit);
        $t['totalData'] = $this->mmoda->get_num_data();

        $create_paging = '';
        if($t['totalData'] > 0){
            $create_paging = $this->libpgn->pagination($t['totalData'],$this->limit);
        }
        $t['create_paging'] = $create_paging;

        $a['content']['content'] = $this->load->view('masterdata/moda/vmoda', $t, TRUE);

        $this->load->view('pages/vmoda_pages', $a, FALSE);
    }

    function add(){
        if($this->input->post()){
            $msg = '';
            if($this->input->post('id_jenis') == NULL){ $msg .= "<p>Jenis kosong</p>"; }
            if($this->input->post('moda') == NULL){ $msg .= "<p>Moda kosong</p>"; }
            if($this->input->post('slot') == NULL){ $msg .= "<p>Slot kosong</p>"; }
            if($this->input->post('id_jenis') == NULL || $this->input->post('moda') == NULL || $this->input->post('slot') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class());
            }

            /*check apakah moda sudah ada*/
            $check = $this->mmoda->check_exist($this->input->post('moda'));
            if($check > 0){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Moda <strong>".$this->input->post('moda')."</strong> sudah ada"));
                redirect(base_url().$this->router->fetch_class());
            }

            $this->mmoda->insert($this->input->post('id_jenis'), $this->input->post('moda'), $this->input->post('slot'), 0);
            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Data berhasil tersimpan"));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Tambah Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $t['list_jenis'] = $this->mjenismoda->get_data_aktif();

        $a['content']['content'] = $this->load->view('masterdata/moda/vadd', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vmoda_pages', $a, FALSE);
    }

    function edit(){
        $id = $this->uri->segment(3);
        if(empty($id)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Uri tidak diketahui"));
            redirect(base_url().$this->router->fetch_class());
        }

        if($this->input->post()){
            $msg = '';
            if($this->input->post('id_jenis') == NULL){ $msg .= "<p>Jenis kosong</p>"; }
            if($this->input->post('moda') == NULL){ $msg .= "<p>Moda kosong</p>"; }
            if($this->input->post('slot') == NULL){ $msg .= "<p>Slot kosong</p>"; }
            if($this->input->post('id_jenis') == NULL || $this->input->post('moda') == NULL || $this->input->post('slot') == NULL){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed",$msg));
                redirect(base_url().$this->router->fetch_class().'/edit/'.$id);
            }

            /*cek moda sudah ada atau belum*/
            $check_moda = $this->mmoda->check_exist_edit($this->input->post('moda'), $id);
            if(!empty($check_moda)){
                $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Moda <strong>".$this->input->post('moda')."</strong> sudah ada yang gunakan"));
                redirect(base_url().$this->router->fetch_class()."/edit/".$id);
            }

            $this->mmoda->update($this->input->post('id_jenis'), $this->input->post('moda'), $this->input->post('slot'), $this->input->post('status'), $id);
            $this->session->set_flashdata("msg", $this->alert->alertMsg("success","Update berhasil ".$msg));
            redirect(base_url().$this->router->fetch_class());
        }

        $a['html']['title'] = 'Edit Data '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/bootstrap-select/css/bootstrap-select.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-validation/jquery.validate.js');
        $a['html']['js_content'] .= add_js('js/form-validation.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-select/js/bootstrap-select.js');

        $get_detail = $this->mmoda->get_data_by_id($id);
        if(empty($get_detail)){
            $this->session->set_flashdata("msg", $this->alert->alertMsg("failed","Data tidak ada dalam database"));
            redirect(base_url().$this->router->fetch_class());
        }
        $t['list'] = $get_detail;
        $t['list_jenis'] = $this->mjenismoda->get_data_aktif();

        $a['content']['content'] = $this->load->view('masterdata/moda/vedit', $t, TRUE);
        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft',NULL,TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);
        $this->load->view('pages/vmoda_pages', $a, FALSE);
    }

}