<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("mdashboard");
    }

    function index()
    {
        $a['html']['title'] = 'Dashboard '.$this->config->item('title_web');

        $a['html']['css'] = add_css('css/fonts/roboto.css');
        $a['html']['css'] .= add_css('css/fonts/material-icons.css');
        $a['html']['css'] .= add_css('plugins/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('plugins/node-waves/waves.css');
        $a['html']['css'] .= add_css('css/style.css');
        $a['html']['css'] .= add_css('css/themes/all-themes.css');
        $a['html']['css'] .= add_css('plugins/animate-css/animate.min.css');

        $a['html']['js_head'] = add_js('plugins/jquery/jquery.min.js');

        $a['html']['js_content'] = add_js('plugins/bootstrap/js/bootstrap.js');
        $a['html']['js_content'] .= add_js('plugins/jquery-slimscroll/jquery.slimscroll.js');
        $a['html']['js_content'] .= add_js('plugins/node-waves/waves.js');
        $a['html']['js_content'] .= add_js('plugins/bootstrap-notify/bootstrap-notify.js');
        $a['html']['js_content'] .= add_js('js/admin.js');
        $a['html']['js_content'] .= add_js('js/demo.js');

        $a['template']['header'] = $this->load->view('template/vheader', NULL, TRUE);
        $a['template']['sidebarleft'] = $this->load->view('template/vsidebarleft', NULL, TRUE);
        $a['template']['sidebarright'] = $this->load->view('template/vsidebarright', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss', NULL, TRUE);

        $a['content']['content'] = $this->load->view('dashboard/vdashboard', NULL, TRUE);

        $this->load->view('pages/vdashboard_pages', $a, FALSE);
    }

    function jumlahdata()
    {
        $t['moda'] = $this->mdashboard->num_moda();
        $t['transportasi'] = $this->mdashboard->num_transportasi();
        $t['karyawan'] = $this->mdashboard->num_karyawan();
        $t['booking_acc'] = $this->mdashboard->num_booking(0);
        $t['booking_reject'] = $this->mdashboard->num_booking(1);
        $t['booking_order'] = $this->mdashboard->num_booking(2);
        $this->load->view('dashboard/vjumlahdata', $t);
    }

    function jumlahpenumpang()
    {
        $t['list'] = $this->mdashboard->get_booking_transportasi();
        $this->load->view('dashboard/vjumlahpenumpang', $t);
    }
}