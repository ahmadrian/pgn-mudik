-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Jul 2020 pada 02.39
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pgn_mudik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_absen`
--

CREATE TABLE `t_absen` (
  `id_absen` int(50) NOT NULL,
  `id_booking` int(50) DEFAULT NULL,
  `check_in` datetime DEFAULT NULL,
  `check_out` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_absen`
--

INSERT INTO `t_absen` (`id_absen`, `id_booking`, `check_in`, `check_out`) VALUES
(2, 1, '2020-07-18 02:38:08', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_booking`
--

CREATE TABLE `t_booking` (
  `id_booking` int(50) NOT NULL,
  `id_karyawan` int(50) DEFAULT NULL,
  `id_transportasi` int(50) DEFAULT NULL,
  `jumlah_penumpang` int(50) DEFAULT NULL,
  `status` int(50) DEFAULT NULL COMMENT '0 acc, 1 reject, 2 order',
  `keterangan` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_booking`
--

INSERT INTO `t_booking` (`id_booking`, `id_karyawan`, `id_transportasi`, `jumlah_penumpang`, `status`, `keterangan`) VALUES
(1, 2, 1, 5, 0, ''),
(3, 1, 1, 5, 2, ''),
(2, 2, 2, 10, 1, 'tidak ada penumpang\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_detail_booking`
--

CREATE TABLE `t_detail_booking` (
  `id_detail_booking` int(50) NOT NULL,
  `id_booking` int(50) DEFAULT NULL,
  `nama_penumpang` varchar(255) DEFAULT NULL,
  `jenis_kelamin` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_detail_booking`
--

INSERT INTO `t_detail_booking` (`id_detail_booking`, `id_booking`, `nama_penumpang`, `jenis_kelamin`) VALUES
(1, 1, 'azis', 1),
(2, 1, 'arif', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_jadwal`
--

CREATE TABLE `t_jadwal` (
  `id_jadwal` int(50) NOT NULL,
  `jadwal` varchar(255) DEFAULT NULL,
  `status` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_jadwal`
--

INSERT INTO `t_jadwal` (`id_jadwal`, `jadwal`, `status`) VALUES
(1, '07:00 WIB', 0),
(2, '13:00 WIB', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_jenis_moda`
--

CREATE TABLE `t_jenis_moda` (
  `id_jenis_moda` int(50) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `status` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_jenis_moda`
--

INSERT INTO `t_jenis_moda` (`id_jenis_moda`, `jenis`, `status`) VALUES
(1, 'Bus', 0),
(2, 'Kapal', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_karyawan`
--

CREATE TABLE `t_karyawan` (
  `id_karyawan` int(50) NOT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `karyawan` varchar(255) DEFAULT NULL,
  `divisi` varchar(255) DEFAULT NULL,
  `jenis_kelamin` int(50) DEFAULT NULL COMMENT '1 pria, 2 wanita',
  `email` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_karyawan`
--

INSERT INTO `t_karyawan` (`id_karyawan`, `id_card`, `karyawan`, `divisi`, `jenis_kelamin`, `email`) VALUES
(1, 'B001', 'Soesasetyo Bimo Tri Amboro', 'TI', 2, 'bimo.amboro@pgn.co.id'),
(2, 'A001', 'Moh Ali', 'TI', 1, 'moh.ali@pgncom.co.id'),
(3, 'C002', 'Chyntia Rahmi Lubis', 'HRD', 1, 'chyntiar@pgncom.co.id'),
(4, 'C001', 'Chaisava Yunda M', 'HRD', 2, 'chaisava.yunda@pgn-perkasa.co.id');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_moda`
--

CREATE TABLE `t_moda` (
  `id_moda` int(50) NOT NULL,
  `id_jenis` int(50) NOT NULL,
  `moda` varchar(255) DEFAULT NULL,
  `slot` int(50) DEFAULT NULL,
  `status` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_moda`
--

INSERT INTO `t_moda` (`id_moda`, `id_jenis`, `moda`, `slot`, `status`) VALUES
(1, 1, 'Damri', 40, 0),
(2, 2, 'Kapal 01', 200, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_role`
--

CREATE TABLE `t_role` (
  `id_role` int(50) NOT NULL,
  `rolename` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_role`
--

INSERT INTO `t_role` (`id_role`, `rolename`) VALUES
(1, 'super admin'),
(2, 'admin humas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rute`
--

CREATE TABLE `t_rute` (
  `id_rute` int(50) NOT NULL,
  `rute` varchar(255) DEFAULT NULL,
  `status` int(50) DEFAULT NULL COMMENT '0 aktif, 1 tidak aktif'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_rute`
--

INSERT INTO `t_rute` (`id_rute`, `rute`, `status`) VALUES
(1, 'Jakarta', 0),
(2, 'Cilegon', 0),
(3, 'Bandung', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_transportasi`
--

CREATE TABLE `t_transportasi` (
  `id_transportasi` int(50) NOT NULL,
  `id_jenis_moda` int(50) DEFAULT NULL,
  `id_moda` int(50) DEFAULT NULL,
  `rute_awal` int(50) DEFAULT NULL,
  `rute_tujuan` int(50) DEFAULT NULL,
  `id_jadwal` int(50) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` int(50) DEFAULT NULL COMMENT '0 tersedia, 1 penuh'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_transportasi`
--

INSERT INTO `t_transportasi` (`id_transportasi`, `id_jenis_moda`, `id_moda`, `rute_awal`, `rute_tujuan`, `id_jadwal`, `content`, `status`) VALUES
(1, 1, 1, 1, 3, 1, 'Harap menjaga kebersihan bus.<br />Terima Kasih.', 0),
(2, 2, 2, 1, 3, 2, '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(50) NOT NULL,
  `id_role` int(50) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `status` int(50) DEFAULT NULL COMMENT '0 aktif, 1 block'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_user`
--

INSERT INTO `t_user` (`id_user`, `id_role`, `username`, `fullname`, `password`, `status`) VALUES
(2, 1, 'superadmin', 'Super Administrator', '4897d9a2a33260732a90552b834a4734', 0),
(3, 2, 'adminhumas', 'Admin Humas', 'fb94ab45f7c19ba78ae62c619ff5d334', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `t_absen`
--
ALTER TABLE `t_absen`
  ADD PRIMARY KEY (`id_absen`);

--
-- Indeks untuk tabel `t_booking`
--
ALTER TABLE `t_booking`
  ADD PRIMARY KEY (`id_booking`);

--
-- Indeks untuk tabel `t_detail_booking`
--
ALTER TABLE `t_detail_booking`
  ADD PRIMARY KEY (`id_detail_booking`);

--
-- Indeks untuk tabel `t_jadwal`
--
ALTER TABLE `t_jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indeks untuk tabel `t_jenis_moda`
--
ALTER TABLE `t_jenis_moda`
  ADD PRIMARY KEY (`id_jenis_moda`);

--
-- Indeks untuk tabel `t_karyawan`
--
ALTER TABLE `t_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `t_moda`
--
ALTER TABLE `t_moda`
  ADD PRIMARY KEY (`id_moda`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indeks untuk tabel `t_role`
--
ALTER TABLE `t_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `t_rute`
--
ALTER TABLE `t_rute`
  ADD PRIMARY KEY (`id_rute`),
  ADD KEY `id_rute` (`id_rute`);

--
-- Indeks untuk tabel `t_transportasi`
--
ALTER TABLE `t_transportasi`
  ADD PRIMARY KEY (`id_transportasi`),
  ADD KEY `status` (`status`);

--
-- Indeks untuk tabel `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `t_absen`
--
ALTER TABLE `t_absen`
  MODIFY `id_absen` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_booking`
--
ALTER TABLE `t_booking`
  MODIFY `id_booking` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `t_detail_booking`
--
ALTER TABLE `t_detail_booking`
  MODIFY `id_detail_booking` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_jadwal`
--
ALTER TABLE `t_jadwal`
  MODIFY `id_jadwal` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_jenis_moda`
--
ALTER TABLE `t_jenis_moda`
  MODIFY `id_jenis_moda` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_karyawan`
--
ALTER TABLE `t_karyawan`
  MODIFY `id_karyawan` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `t_moda`
--
ALTER TABLE `t_moda`
  MODIFY `id_moda` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_role`
--
ALTER TABLE `t_role`
  MODIFY `id_role` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_rute`
--
ALTER TABLE `t_rute`
  MODIFY `id_rute` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `t_transportasi`
--
ALTER TABLE `t_transportasi`
  MODIFY `id_transportasi` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
